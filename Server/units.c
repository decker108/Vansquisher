/*
 * units.c
 *
 *  Created on: 1 apr 2010
 *      Author: Petter
 */

#include "units.h"
#include <stdlib.h>
#include <time.h>

int combatMap[14][18] = {{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
						 {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
						 {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
						 {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
						 {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
						 {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
						 {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
						 {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
						 {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
						 {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
						 {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
						 {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
						 {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
						 {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0} };

struct unit* createUnit(int xPos, int yPos, int type, int id){

	struct unit *u;
	//Reservera plats i minnet f�r structen
	u = (struct unit*)calloc(1,sizeof(struct unit));

	u->x = xPos;
	u->y = yPos;
	u->uID = id;
	u->dead = 0;

	switch (type){
        case SOLDIER:
            u->width = 64;
            u->height = 64;
            u->type = SOLDIER;
            u->pwr = 1;
            u->movePts = 2;
            break;
        case TANK:
            u->width = 64;
            u->height = 64;
            u->type = TANK;
            u->pwr = 2;
            u->movePts = 3;
            break;
        case COPTER:
            u->width = 64;
            u->height = 64;
            u->type = COPTER;
            u->pwr = 2;
            u->movePts = 3;
            break;
        default:;
    }
	return u;
}

int spawnUnits(struct unit* units[], int map)
{
    int numberOfUnits;
    switch(map){
        case 1:
            units[0] = createUnit(64*1,64*3, SOLDIER,10);
            units[1] = createUnit(64*2,64*3, SOLDIER,11);
            units[2] = createUnit(64*2,64*4, SOLDIER,12);
            units[3] = createUnit(64*0,64*5, TANK,13);
            units[4] = createUnit(64*3,64*5, TANK,14);
            units[5] = createUnit(64*4,64*4, COPTER, 15);

            units[6] = createUnit(64*14,64*10, SOLDIER,20);
            units[7] = createUnit(64*13,64*10, SOLDIER,21);
            units[8] = createUnit(64*13,64*9, SOLDIER,22);
            units[9] = createUnit(64*12,64*8, TANK,23);
            units[10] = createUnit(64*12,64*7, TANK,24);
            units[11] = createUnit(64*15,64*8, COPTER,25);

            numberOfUnits = 12;
            break;
        case 2:
            units[0] = createUnit(64*1,64*3, SOLDIER,10);
            units[1] = createUnit(64*2,64*3, SOLDIER,11);
            units[2] = createUnit(64*3,64*4, SOLDIER,12);
            units[3] = createUnit(64*0,64*5, SOLDIER,13);
            units[4] = createUnit(64*4,64*5, TANK,14);
            units[5] = createUnit(64*4,64*3, TANK,15);
            units[6] = createUnit(64*4,64*4, COPTER, 16);

            units[7] = createUnit(64*14,64*10, SOLDIER,20);
            units[8] = createUnit(64*13,64*10, SOLDIER,21);
            units[9] = createUnit(64*12,64*9, SOLDIER,22);
            units[10] = createUnit(64*13,64*8, SOLDIER,23);
            units[11] = createUnit(64*12,64*8, TANK,24);
            units[12] = createUnit(64*12,64*7, TANK,25);
            units[13] = createUnit(64*15,64*8, COPTER,26);

            units[14] = createUnit(64*2,64*10, SOLDIER,30);
            units[15] = createUnit(64*1,64*10, SOLDIER,31);
            units[16] = createUnit(64*3,64*9, SOLDIER,32);
            units[17] = createUnit(64*4,64*9, SOLDIER,33);
            units[18] = createUnit(64*0,64*9, TANK,34);
            units[19] = createUnit(64*3,64*8, TANK,35);
            units[20] = createUnit(64*5,64*9, COPTER,36);

            numberOfUnits = 21;
            break;
        case 3:
            units[0] = createUnit(64*1,64*3, SOLDIER,10);
            units[1] = createUnit(64*2,64*3, SOLDIER,11);
            units[2] = createUnit(64*3,64*4, SOLDIER,12);
            units[3] = createUnit(64*0,64*5, SOLDIER,13);
            units[4] = createUnit(64*4,64*5, TANK,14);
            units[5] = createUnit(64*4,64*3, TANK,15);
            units[6] = createUnit(64*4,64*4, COPTER, 16);

            units[7] = createUnit(64*14,64*10, SOLDIER,20);
            units[8] = createUnit(64*13,64*10, SOLDIER,21);
            units[9] = createUnit(64*13,64*9, SOLDIER,22);
            units[10] = createUnit(64*13,64*8, SOLDIER,23);
            units[11] = createUnit(64*12,64*8, TANK,24);
            units[12] = createUnit(64*13,64*7, TANK,25);
            units[13] = createUnit(64*15,64*8, COPTER,26);

            units[14] = createUnit(64*2,64*10, SOLDIER,30);
            units[15] = createUnit(64*1,64*10, SOLDIER,31);
            units[16] = createUnit(64*3,64*9, SOLDIER,32);
            units[17] = createUnit(64*4,64*9, SOLDIER,33);
            units[18] = createUnit(64*0,64*8, TANK,34);
            units[19] = createUnit(64*3,64*8, TANK,35);
            units[20] = createUnit(64*4,64*8, COPTER,36);

            units[21] = createUnit(64*14,64*0, SOLDIER,40);
            units[22] = createUnit(64*13,64*1, SOLDIER,41);
            units[23] = createUnit(64*13,64*0, SOLDIER,42);
            units[24] = createUnit(64*13,64*2, SOLDIER,43);
            units[25] = createUnit(64*12,64*3, TANK,44);
            units[26] = createUnit(64*12,64*1, TANK,45);
            units[27] = createUnit(64*15,64*0, COPTER,46);

            numberOfUnits = 28;
            break;
        case 4:
            break;
    }
    return numberOfUnits;
}

void resetUnitData(struct unit* u[], int noOfUnits)
{
    int i;
    for(i = 0; i < noOfUnits; i++){
        u[i]->movePts = 2;
    }

}

void moveUnit(struct unit *u, int direction)
{
    //�ndra enhetens x/y coordinarer beroende p� direction
    switch(direction){
		case UNIT_MOVE_UP:
			u->y -= UNIT_PIXEL_MOVE;
			break;
		case UNIT_MOVE_DOWN:
			u->y += UNIT_PIXEL_MOVE;
			break;
		case UNIT_MOVE_RIGHT:
			u->x += UNIT_PIXEL_MOVE;
			break;
		case UNIT_MOVE_LEFT:
			u->x -= UNIT_PIXEL_MOVE;
			break;
		default:;
	}
}
int checkBounds(struct unit *u, int direction)
{
    //Kontrollera om en f�rflyttning tar spelaren utanf�r sk�rmen
	switch(direction){
        case UNIT_MOVE_UP:				//UP
            if(u->y-UNIT_PIXEL_MOVE < 0)
                return 0;
            break;
        case UNIT_MOVE_DOWN:				//DOWN
            if(u->y+u->height+UNIT_PIXEL_MOVE > LEVEL_HEIGHT)
                return 0;
            break;
        case UNIT_MOVE_RIGHT:				//RIGHT
            if(u->y+u->width+UNIT_PIXEL_MOVE > LEVEL_WIDTH)
                return 0;
            break;
        case UNIT_MOVE_LEFT:				//LEFT
            if(u->x-UNIT_PIXEL_MOVE < 0)
                return 0;
            break;
    }
	return 1;
}

int checkMapBounds(struct unit *u, int direction, int map[LEVEL_TILES_COL][LEVEL_TILES_ROW]){
	int unitXCoord = u->x/UNIT_PIXEL_MOVE;
	int unitYCoord = u->y/UNIT_PIXEL_MOVE;
	int tileNo, i, obsLength;

    //De tiles som spelaren inte f�r flytta in enheter i
    int obstacles[6] = {20};
//	int obstacles[6] = {1,2,3,4,5,6};

	obsLength = sizeof(obstacles)/sizeof(int);

    //H�mta den tile som spelaren f�rs�ker flytta till
	switch(direction){
	case UNIT_MOVE_UP:
		tileNo = map[unitYCoord-1][unitXCoord];
		break;
	case UNIT_MOVE_DOWN:
		tileNo = map[unitYCoord+1][unitXCoord];
		break;
	case UNIT_MOVE_RIGHT:
		tileNo = map[unitYCoord][unitXCoord+1];
		break;
	case UNIT_MOVE_LEFT:
		tileNo = map[unitYCoord][unitXCoord-1];
		break;
	}

    //Kontrollera om denna tile finns i obstacles[]
	for(i = 0; i < obsLength; i++){
		if(tileNo == obstacles[i] && u->type != COPTER){
			return 0;
		}
	}
	return 1;
}

void combatMapSet(struct unit *u){
	int unitXCoord = u->x/UNIT_PIXEL_MOVE;
	int unitYCoord = u->y/UNIT_PIXEL_MOVE;
	int i, x;

	combatMap[unitYCoord][unitXCoord] = u->uID;
	for(i = 0; i < 14; i++){
        for(x = 0; x < 18; x++){
            printf("%d ", combatMap[i][x]);
        }
        printf("\n");
	}
    printf("\n");

}
void combatMapClear(struct unit *u){
	int unitXCoord = u->x/UNIT_PIXEL_MOVE;
	int unitYCoord = u->y/UNIT_PIXEL_MOVE;

	combatMap[unitYCoord][unitXCoord] = 0;
}

int checkUnitCollision(struct unit *u, int direction){
	int unitXCoord = u->x/UNIT_PIXEL_MOVE;
	int unitYCoord = u->y/UNIT_PIXEL_MOVE;
	int attackThis;

	//Kontrollera om rutan till (direction) om spelaren �r upptagen
	switch(direction){
	case UNIT_MOVE_UP:
		attackThis = combatMap[unitYCoord-1][unitXCoord];
		break;
	case UNIT_MOVE_DOWN:
		attackThis = combatMap[unitYCoord+1][unitXCoord];
		break;
	case UNIT_MOVE_RIGHT:
		attackThis = combatMap[unitYCoord][unitXCoord+1];
		break;
	case UNIT_MOVE_LEFT:
		attackThis = combatMap[unitYCoord][unitXCoord-1];
		break;
	}

	if(attackThis != 0 && attackThis /10 != (u->uID)/10){
            //Returnera uID f�r den attackerade enheten (h�mtas fr�n combatMap)
			return attackThis;
	}
	else if(attackThis != 0){
		//Spelaren ska inte kunna flytta in i en ruta med egna soldater
		return 0;
	}
	//Om spelaren inte gick till samma ruta som en annan enhet
	return 1;
}

int initCombat(struct unit *attacker, struct unit *defender, int map[LEVEL_TILES_COL][LEVEL_TILES_ROW])
{
	int attRoll, defRoll, attBonus, defBonus;
	int terrainAtt, terrainDef;
	int attXCoord, attYCoord, defXCoord, defYCoord;
	srand(time(0));

	//Koordinaterna p� kartan beh�vs f�r att hitta r�tt terr�ng
	attXCoord = attacker->x/UNIT_PIXEL_MOVE;
	attYCoord = attacker->y/UNIT_PIXEL_MOVE;

	defXCoord = defender->x/UNIT_PIXEL_MOVE;
	defYCoord = defender->y/UNIT_PIXEL_MOVE;


	//Loopen g�r igenom alla enheter tills den hittar uID p� den enheten som attackeras
			//Koordinaterna p� kartan beh�vs f�r att hitta r�tt terr�ng
			terrainAtt = map[attYCoord][attXCoord];
			terrainDef = map[defYCoord][defXCoord];

			attBonus = terrainCombatBonus(terrainAtt, attacker) + attacker->pwr;
			defBonus = terrainCombatBonus(terrainDef, defender) + defender->pwr;

			attRoll = (rand()%6+1) + attBonus;
			defRoll = (rand()%6+1) + defBonus;

			printf("\n!!%d ATTACKS %d from terrain %d to terrain %d!!\n", attacker->uID, defender->uID,terrainAtt, terrainDef);

			printf("Attacker Rolled: %d\nDefender rolled: %d\n", attRoll,defRoll);

			if(attRoll > defRoll){
				defender->dead = 1;
				combatMapClear(defender);
				return defender->uID;
				printf("Attacker wins\n");
			}
			if(defRoll >= attRoll){
				attacker->dead = 1;
				combatMapClear(attacker);
				return attacker->uID;
				printf("Defender wins\n");
			}
			fflush(stdout);

			return -1;
}

void resetMovePts(struct unit* units[], int noOfUnits)
{
    int i;
    for(i = 0; i < noOfUnits; i++){
        if(units[i]->dead == 0){
            if(units[i]->type == SOLDIER){
                units[i]->movePts = 2;
            }
            if(units[i]->type == TANK || units[i]->type == COPTER){
                units[i]->movePts = 3;
            }
        }
    }
}

int terrainCombatBonus(int terrain,struct unit *combatant)
{
    int bonus;
    switch(combatant->type){
        case SOLDIER:
            if(terrain == GRASS || (terrain >= ROAD_START && terrain <= ROAD_END) || terrain == MUD)
                bonus = 0;
            if(terrain == SNOW || terrain == DESERT || terrain == SWAMP)
                bonus = -2;
            if(terrain == WOODS || terrain == CITY || terrain == MOUNTAIN)
                bonus = 2;
            break;
        case TANK:
            if(terrain >= ROAD_START && terrain <= ROAD_END)
                bonus = 2;
            if(terrain == SWAMP || terrain == MOUNTAIN || terrain == WOODS)
                bonus = -2;
            if(terrain == GRASS || terrain == MUD || terrain == CITY || terrain == SNOW || terrain == DESERT)
                bonus = 0;
            break;
        case COPTER:
            if(terrain == GRASS || terrain == MUD || terrain == SNOW ||
                    terrain == DESERT || (terrain >= ROAD_START && terrain <= ROAD_END))
                    bonus = 2;
            if(terrain == CITY || terrain == WOODS || terrain == SWAMP)
                    bonus = -2;
            if(terrain == MOUNTAIN || terrain == WATER)
                    bonus = 0;
            break;
    }
    return bonus;
}

int findUnitWithID(struct unit *allUnits[], int id, int noOfUnits)
{
	int i;
	for(i = 0; i < noOfUnits; i++){
		if(allUnits[i]->uID == id){
			return i;
		}
	}
	//Om enheten inte kunde hittas (Fel har uppst�tt)
	return -1;
}
