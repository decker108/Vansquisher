/*************************/
/**     Skriven av      **/
/** Alexander Bj�rklund **/
/**     2010-05-12      **/
/**                     **/
/**    Reviderad av    **/
/** Alexander Bj�rklund **/
/**     2010-05-17      **/
/*************************/

#include <string.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include "SDL/SDL.h"
#include "SDL/SDL_net.h"

#include "chat.h"
#include "textformat.h"

int num_clients=0;
TCPsocket server;

SDLNet_SocketSet create_sockset(Client clients[4]);

int chat_main(void *baraForSakensSkull)
{
    IPaddress ip;
	TCPsocket sock;
	SDLNet_SocketSet set;
	char *message=NULL;
	char *str=NULL;
	const char *host=NULL;
	Uint32 ipaddr;
	Uint16 port=5666;
	Client clients[4];

    initClients(clients);


	/* Resolve the argument into an IPaddress type */
	if(SDLNet_ResolveHost(&ip,NULL,port)==-1)
	{
		printf("SDLNet_ResolveHost: %s\n",SDLNet_GetError());
		SDLNet_Quit();
		SDL_Quit();
		return 3;//exit(3);
	}

	/* perform a byte endianess correction for the next printf */
	ipaddr=SDL_SwapBE32(ip.host);

	/* output the IP address nicely */
	printf("IP Address : %d.%d.%d.%d\n",
			ipaddr>>24,
			(ipaddr>>16)&0xff,
			(ipaddr>>8)&0xff,
			ipaddr&0xff);

	/* resolve the hostname for the IPaddress */
	host=SDLNet_ResolveIP(&ip);

	/* print out the hostname we got */
	if(host)
		printf("Hostname   : %s\n",host);
	else
		printf("Hostname   : N/A\n");

	/* output the port number */
	printf("Port       : %d\n",port);

	/* open the server socket */
	server=SDLNet_TCP_Open(&ip);
	if(!server)
	{
		printf("SDLNet_TCP_Open: %s\n",SDLNet_GetError());
		SDLNet_Quit();
		SDL_Quit();
		return 4; //exit(4);
	}

   	while(1)
	{
	    printf("C");
		int numready,i;
		set=create_sockset(clients);
		numready=SDLNet_CheckSockets(set, (Uint32)-1);

        if(numready==-1)
            {
                printf("SDLNet_CheckSockets: %s\n",SDLNet_GetError());
                break;
            }
            if(!numready)
                continue;

            if(SDLNet_SocketReady(server) && (num_clients < 4))
            {
                numready--;
                /*printf("Connection...\n"); */
                sock=SDLNet_TCP_Accept(server);
                if(sock)
                {
                    char *name=NULL;

                    /*printf("Accepted...\n"); */
                    if(recvM(sock, &name))
                    {
                        add_client(sock, name, clients);
                    }
                    else
                		SDLNet_TCP_Close(sock);
                }
            }

            for(i=0; i< 4; i++){
                if(clients[i].inUse == 1){

                    if(SDLNet_SocketReady(clients[i].sock))
                    {
                        if(recvM(clients[i].sock, &message))
                        {
                            numready--;
                            printf("<%s> %s\n",clients[i].name,message);
                            /*interpret commands */
                            if(message[0]=='/' && strlen(message)>1)
                            {
                            gor_kommando(clients, message, i );
                            }
                            else // it's a regular message
                            {
                                /* forward message to ALL clients... */
                                str=formatText("ssss","<",clients[i].name,"> ",message);
                                if(str)
                                    send_all(str, clients);
                            }
                            free(message);
                            message=NULL;
                        }
                        else{
                            remove_client(i, clients);
                        }
                    }
                }
            }

	}

    return 0;
}

void add_client(TCPsocket sock, char *name, Client clients[4])
{
    int i;
    for(i=0; i<4; i++){
        if(clients[i].inUse == 0){
            clients[i].name=name;
            clients[i].sock=sock;
            clients[i].inUse=1;
            num_clients++;
            break;
        }
    }

	/* server side info */
	printf("--> %s\n",name);
	/* inform all clients, including the new one, of the joined user */
	who(clients, i);
	send_all(formatText("ss","--> ",name), clients);
}

void remove_client(int i, Client clients[4])
{
	char *name=clients[i].name;

	if(i<0)
		return;

	/* close the old socket, even if it's dead... */
	SDLNet_TCP_Close(clients[i].sock);

    clients[i].sock = NULL;
	num_clients--;
	clients[i].inUse=0;

	/* server side info */
	printf("<-- %s\n",name);
	send_all(formatText("sss", "Client ", clients[i].name, " has disconnected. The game is over"), clients);
	/* inform all clients, excluding the old one, of the disconnected user */
	//send_all(formatText("ss","<-- ",name));
	if(name)
		free(name);
}

SDLNet_SocketSet create_sockset(Client clients[4])
{
	static SDLNet_SocketSet set=NULL;
	int i;

	if(set)
		SDLNet_FreeSocketSet(set);
	set=SDLNet_AllocSocketSet(num_clients+1);
	if(!set) {
		printf("SDLNet_AllocSocketSet: %s\n", SDLNet_GetError());
		return 1;//exit(1); /*most of the time this is a major error, but do what you want. */
	}
	SDLNet_TCP_AddSocket(set,server);
	for(i=0;i<4;i++){
	    if(clients[i].inUse==1)
            SDLNet_TCP_AddSocket(set,clients[i].sock);
	}
	return(set);
}

void send_all(char *buf, Client clients[4])
{
	int i;

	if(!buf || !num_clients)
		return;

	for(i=0; i<4; i++)
	{
		/* sendM is in tcputil.h, it sends a buffer over a socket */
		/* with error checking */
		if(clients[i].inUse == 1){
            if(sendM(clients[i].sock,buf))
                ;
            else
                remove_client(i, clients);
		}
	}
}

void initClients(Client clients[4]){
    int i;

    for(i=0; i<4; i++){
        clients[i].inUse=0;
    }
}

void gor_kommando(Client clients[4], char *str, int cindex){
    if(!strcasecmp(str, "/QUIT")){
        remove_client(cindex, clients);
        return;
    }else if(!strcasecmp(str, "/DANCE")){
        send_all(formatText("ss", clients[cindex].name, " dances."),  clients);
        return;
    }else if(!strcasecmp(str, "/NEXT")){
        send_all(formatText("sds", "Player ", cindex+1, " has ended his turn."),  clients);
        return;
    }else if(!strcasecmp(str, "/WHO")){
        who(clients, cindex);
        return;
    }else if(!strcasecmp(str, "/COOKIE")){
        send_all(formatText("ss", clients[cindex].name, " eats a cookie!"),  clients);
        return;
    }else if(!strcasecmp(str, "/GOD")){
        send_all(formatText("ss", clients[cindex].name, " has activated Godmode"),  clients);
        return;
    }else if(!strcasecmp(str, "/NOCLIP")){
        send_all(formatText("ss", clients[cindex].name, " has activated Noclip"),  clients);
        return;
    }else if(!strcasecmp(str, "/IMPULSE9")){
        send_all(formatText("ss", clients[cindex].name, " thinks he's playing Quake"),  clients);
        return;
    }else if(!strcasecmp(str, "/GIVELORDAREKUYOURSOUL")){
        send_all(formatText("ss", clients[cindex].name, " surrenders his soul to "),  clients);
        send_all("Lord Areku, Rex Imperator!",  clients);
        return;
    }else if(!strcasecmp(str, "/SPAMMERHELL")){
        int i;
        for(i=0; i<3; i++){
            send_all(formatText("ss", clients[cindex].name, " is spamming the chat to death!"),  clients);
        }
        return;
    }else if(!strcasecmp(str, "/IP")){
        sendM(clients[cindex].sock, getIP(clients[cindex].sock));
        return;
    }else if(!strcasecmp(str, "/HELP")){
        sendM(clients[cindex].sock, "--- Available commands / emotes ");
        sendM(clients[cindex].sock, "--- Commands: ");
        sendM(clients[cindex].sock, "--- /WHO - prints a list of connected users. ");
        sendM(clients[cindex].sock, "--- Emotes: ");
        sendM(clients[cindex].sock, "--- /DANCE ");
        sendM(clients[cindex].sock, "--- /COOKIE ");
        return;
    }else {
        sendM(clients[cindex].sock, "--- Type /HELP for a list of commands ");
    }

}

void who(Client clients[4], int cindex){
    int i;
    char *ip;
    sendM(clients[cindex].sock, "Online:");
    for(i=0; i<4; i++){
        if(clients[i].inUse == 1){
            ip=getIP(clients[i].sock);
            printf("%s\n", ip);
            sendM(clients[cindex].sock, formatText("s", clients[i].name));
        }
    }
}



char *recvM(TCPsocket sock, char **buf) {
    Uint32 len,result;
    static char *_buf;

    if (!buf)
        buf=&_buf;

    if (*buf)
        free(*buf);
    *buf=NULL;

    result=SDLNet_TCP_Recv(sock,&len,sizeof(len));
   // *ret = result;
    printf("RES: %d", result);
    if (result<sizeof(len)) {
        if (SDLNet_GetError() && strlen(SDLNet_GetError()))
            printf("SDLNet_TCP_Recv: %s\n", SDLNet_GetError());
        return(NULL);
    }

    len=SDL_SwapBE32(len);

    if (!len)
        return(NULL);

    *buf=(char*)malloc(len);
    if (!(*buf))
        return(NULL);

    result=SDLNet_TCP_Recv(sock,*buf,len);
    printf("RES: %d", result);

    if (result<len) {
        if (SDLNet_GetError() && strlen(SDLNet_GetError()))
            printf("SDLNet_TCP_Recv: %s\n", SDLNet_GetError());
        free(*buf);
        buf=NULL;
    }

    return(*buf);
}


int sendM(TCPsocket sock, char *buf) {
    Uint32 len,result;

    if (!buf || !strlen(buf))
        return(1);

    len=strlen(buf)+1;

    len=SDL_SwapBE32(len);

    result=SDLNet_TCP_Send(sock,&len,sizeof(len));
    if (result<sizeof(len)) {
        if (SDLNet_GetError() && strlen(SDLNet_GetError()))
            printf("SDLNet_TCP_Send: %s\n", SDLNet_GetError());
        return(0);
    }

    len=SDL_SwapBE32(len);

    result=SDLNet_TCP_Send(sock,buf,len);
    if (result<len) {
        if (SDLNet_GetError() && strlen(SDLNet_GetError()))
            printf("SDLNet_TCP_Send: %s\n", SDLNet_GetError());
        return(0);
    }

    return(result);
}
