/*************************/
/**     Skriven av      **/
/** Alexander Bj�rklund **/
/**     2010-05-12      **/
/**                     **/
/**    Reviderad av    **/
/** Alexander Bj�rklund **/
/**     2010-05-17      **/
/*************************/

#ifndef CHAT_H_
#define CHAT_H_ 1

#include <string.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include "SDL/SDL.h"
#include "SDL/SDL_net.h"
#include "textformat.h"
//#include "tcputil.h"



typedef struct {
	TCPsocket sock;
	char *name;
	int inUse;
} Client;

int chat_main(void *);

//Funktion som tar array av typen Client och en socket samt
//namnet p� anv�ndaren som ansluter och l�gger in dom i
//arrayen om det finns platser kvar med inUse satt till 0
void add_client(TCPsocket sock, char *name, Client clients[4]);

//Tar emot index p� vilken klient som ska tas bort och en array
//med Client och st�nger socketen och frig�r positionen i arrayen
//f�r �teranv�ndning
void remove_client(int i, Client clients[4]);

//Tar ett meddelande och en array av Client och skickar meddelandet
//�ver alla aktiva sockets i arrayen
void send_all(char *buf, Client clients[4]);

//G�r alla platser i arrayen redo f�r anv�ndning
void initClients(Client clients[4]);

//tar emot en array med klienter och en str�ng inneh�llande kommandot
//samt en int som representerar klienten som utf�rde kommandot.
//Och utf�r sedan kommandot
void gor_kommando(Client clients[4], char *str, int cindex);

//tar emot en array av klinter och en int representerande den
//klient som anropat kommandot /WHO, och skickar sedan namnen
//p� alla inloggade klienter till den klient som anges av cindex
void who(Client clients[5], int cindex);

//tar emot en buffer fr�n en TCP socket
//funktionen hanterar minnet s� den kan inte ta emot [] arrayer
//returnerer 0 vid fel eller en giltig *char vid framg�ng
char *recvM(TCPsocket sock, char **buf);

//skickar en str�ng bufer via en TCP socket
//returnerar 0 vid fel, annars den skickade lngden
int sendM(TCPsocket sock, char *buf);

#endif
