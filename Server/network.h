/*
 * network.h
 *      Inneh�ller datatyper f�r kommunikation samt
 *      omf�rdelningsfunktion
 *      Author: Petter L�fgren
 */

#ifndef NETWORK_H_
#define NETWORK_H_
#include "SDL/SDL_net.h"
#include "filhantering_dat.h"

//Struktur som skickas till klienterna d� spelet k�rs
typedef struct{
    //Angert att det g�r bra att flytta enhet
	int okToMove;
	//Indikerar om strid har intr�ffat
	int combat;
	//Den enhet (unitID) som f�rlorade striden
	int combatLoser;
	//Vilken enhet som ska flytta p� sig (Samma som motagen struct)
	int unitID;
	//�t vilken riktning
	int moveDirection;
	//�r 1 om det �r n�sta spelares tur
	int nextPlayerTurn;
	//Indikerar att en spelare har kopplats ner utan krash
	int disconnectedPlayer;
	//Blir 1 d� spelet �r �ver
	int gameOver;
}toClient;

//Den struktur som tas emot fr�n klienten vars tur det �r (under spelets g�ng).
typedef struct{
    //Vilken enhet som ska flyttas
	int unitID;
	//�t vilket h�ll spelaren vill flytta denna
	int direction;
	//Vem spelaren �r
	int player;
	//Indikerar att spelaren har st�ngts av utan krash
	int iDisconnect;
	//Flagga - Anger att spelaren vill avsluta sin tur
    int endMyTurn;
    //Flagga - Anger att spelaren vill skippa en enhet.
	int skipThisUnit;
} recievedFromClient;

//Ska kanske tas bort

struct recievedFromPassive{
    int iQuit;
};

//Struct som tas emot d� spelet �r i lobbyl�ge
typedef struct{
    //Ett om spelare ett v�ljer att starta spelet
    int startGame;
    //Flagga -
    int iDisconnectFromLobby;
    int map;
    char name[4];
} recievedInitData;

//Struct som s�nds till spelarna i lobbyl�ge
typedef struct{
    int playerNo;
    int playerTurn;
    int okToStart;
    int map;
    int playerQuant;
} toClientsLobbyMode;

//Struct med highscore data som skickas till alla klienter
//innan servern startar om.
typedef struct{
    gameClient hs10[10];
} sendHS;

//F�rdelar om socketarna i clients[]
void rearrangeClients(TCPsocket clients[], int clientToDisconnect, int clientsConnected);

#endif
