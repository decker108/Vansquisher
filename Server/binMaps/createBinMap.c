#include <stdio.h>


int main()
{
    FILE *txtFile;
    FILE *binFile;
    int u,i;
    char mapName[40];

    int map[14][18];
    int binMap[14][18];

    printf(".txt file to read from: ");
    scanf("%s", mapName);

    txtFile=fopen(mapName,"rt");

    if(txtFile == NULL){
        printf("Error opening file");
        return -1;
    }


    for (i=0; i<14; i++) {
        for (u=0; u<18; u++) {
            fscanf(txtFile, "%d",&map[i][u]);
        }
    }
    printf("Textfile:\n");
    for (i=0; i<14; i++) {
        for (u=0; u<18; u++) {
            printf("%d ",map[i][u]);
        }
        printf("\n");
    }
    fclose(txtFile);
    printf(".dat file to write to: ");
    scanf("%s", mapName);

    binFile = fopen(mapName, "wb");
    printf("writing\n");
    for (i=0; i<14; i++) {
        for (u=0; u<18; u++) {
            fwrite(&map[i][u], sizeof(int), 1, binFile);
        }
    }
    fclose(binFile);
    binFile=fopen(mapName, "rb");
    printf("reading\n");
    for (i=0; i<14; i++) {
        for (u=0; u<18; u++) {
            fread(&binMap[i][u], sizeof(int), 1, binFile);
        }
    }
    printf("Contents of binary file:\n");
    for (i=0; i<14; i++) {
        for (u=0; u<18; u++) {
            printf("%d ",binMap[i][u]);
        }
        printf("\n");
    }
    fclose(binFile);
    getchar();
    return 0;
}
