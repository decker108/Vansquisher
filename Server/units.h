/*
 * units.h
 *
 *  Created on: 1 apr 2010
 *  Author: Petter
 *
 *  Tar hand om de ber�kningar som har att g�ra med f�rflyttning av
 *  enheter och ser till att enheterna stannar inom sk�rmen och inom
 *  spelplanens gr�nser
 */

#ifndef UNITS_H_
#define UNITS_H_
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "maps.h"
#include <stdlib.h>
#include <stdio.h>

////Sk�rmattribut - (Anv�nds endast f�r testning)
//#define SCREEN_WIDTH 680
//#define SCREEN_HEIGHT 480
//#define SCREEN_BPP 32

//#define WATER 2

//Antalet pixlar enheten r�r sig vid en knapptryckning
#define UNIT_PIXEL_MOVE 64
//Hur m�nga pixlar en enhet r�r sig per bilduppdaterin
#define UNIT_SPEED 2
//Riktningar
#define UNIT_MOVE_UP 0
#define UNIT_MOVE_DOWN 1
#define UNIT_MOVE_RIGHT 2
#define UNIT_MOVE_LEFT 3

//Antalet enheter som finns p� spelplanen
#define MAX_NUMBER_OF_UNITS 40

#define GRASS 1
#define ROAD_START 2
#define ROAD_END 12
#define MUD 13
#define SNOW 14
#define DESERT 15
#define WOODS 16
#define SWAMP 17
#define CITY 18
#define MOUNTAIN 19
#define WATER 20


//Enhetstyper
#define SOLDIER 1
#define TANK 2
#define COPTER 3

//Enhetens attribut
struct unit{
	int x;
	int y;
	int width;
	int height;
	//Typ av enhet - SOLDIER, TANK OSV
	int type;
	//Enhetens unika ID p� spelplanen
	int uID;
	//1 om enheten �r d�d, alltid 0 d� den skapas
	int dead;
	//Enhetens anfall/f�rsvarsstyrka
	int pwr;
	//Antalet steg enheten kan ta i varje runda
	int movePts;
};

/* Skapar en enhet. Initierar v�rden s� som positioner,
 * bredd, h�jd och typ av enhet.
 */
struct unit* createUnit(int xPos, int yPos, int type, int id);

// �terst�ller enhetsdata
//�Terst�ller enhetsdata
void resetUnitData(struct unit* units[], int noOfUnits);

//�Terst�ller alla enheters f�rflyttningspo�ng
void resetMovePts(struct unit* units[], int noOfUnits);

// Placerar ut enheterna p� kartan.
int spawnUnits(struct unit* units[], int map);

/* Flyttar enheten beroende p� knapptryckningar. Tar emot
 * en pekare till strukturen som ska anv�ndas samt ett heltal
 * som anger vilket h�ll enheten ska flyttas �t.
 */
void moveUnit(struct unit *u, int direction);

/* Kontrollerar att spelaren inte g�r utanf�r sk�rmen, om detta
 * intr�ffar flyttas enheten tillbaka lika m�nga steg som den
 * flyttades innan funktionen anropades. Anropas fr�n moveUnit.
 */

int checkBounds(struct unit *u, int direction);

/* Ser till att spelaren inte kan g� p� visa best�mda logiska "tiles"
 * genom att j�mf�ra spelarens position p� sk�rmen mot en matris
 * inneh�llande spelplanen. Anropas fr�n moveUnit
 */
int checkMapBounds(struct unit *u, int direction,int map[LEVEL_TILES_COL][LEVEL_TILES_ROW]);

/* S�tter ut struct unit->uID p� den tile dit spelaren har flyttat
 * sin enhet, f�r att h�lla reda p� vilka enheter som befinner sig vart.
 * Anropas direkt efter f�rflyttningen av en enhet.
 */
void combatMapSet(struct unit *u);

/* S�tter ut 0(noll) p� den tile som enheten l�mnade innan den
 * f�rflyttades. Anv�nds tillsammans med combatMapSet f�r att
 * uppdatera kartan med spelares enheter.
 * Anropas direkt innan f�rflyttningen av en enhet.
 */
void combatMapClear(struct unit *u);

/* Kontrollerar om spelaren f�rs�ker flytta in en enhet till en ruta d�r
 * en annan enhet st�r. Kontrollerar om enheten �r fientlig, och startar i
 * s� fall strid. *u �r pekare till den enhet som flyttas av spelaren
 * direction �r det h�ll �t vilken spelaren valt att g�. Returnerar 1 om
 * ingen kollision uppst�r, 0 om kollision uppst�r med en annan av spelarens
 * enheter. Returnerar uID p� den enhet som spelaren flyttar in i om enheten
 * var fientlig.
 */
int checkUnitCollision(struct unit *u, int direction);

/* Startar och k�r en strid mellan tv� enheter. *attacker �r den attackerande
 * enheten, *defender �r den f�rsvarande.
 */
int initCombat(struct unit *attacker, struct unit *defender, int map[LEVEL_TILES_COL][LEVEL_TILES_ROW]);

/* Returnerar en enhets position i arrayen om enhetens uID �r k�nt
*/
int findUnitWithID(struct unit *allUnits[], int id, int noOfUnits);

/* Mappar olika terr�nger mot olika bonusar vid strid
 */
int terrainCombatBonus(int terrain, struct unit *combatant);
#endif /* UNITS_H_ */
