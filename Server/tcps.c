#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "SDL/SDL.h"
#include "SDL/SDL_net.h"
#include "units.h"
#include "network.h"
#include "chat.h"
#include "maps.h"
#include "filhantering_dat.h"
#include "textformat.h"

#define NUMSOCKET 5

int main(int argc, char *argv[])
{
	TCPsocket server; //Serverns socket deskriptor
	TCPsocket tempClient; //Tempor�r socket f�r varje klient som ansluter
	IPaddress ip; //Datatyp f�r ip-address information

	TCPsocket clients[NUMSOCKET]; //Vektor d�r alla kienters socket finns

	SDLNet_SocketSet socketset = NULL; //Socketset som anv�nds f�r polling

    int map[LEVEL_TILES_COL][LEVEL_TILES_ROW];//Karta med terr�nger

	struct recievedFromPassive passiveData;
 	recievedFromClient recData; //Data som tas emot i spell�ge
	toClient sendData;          //Data som skickas till klienterna i spell�ge
    recievedInitData fromLobby; //Data som tas emot fr�n klienter i lobbyl�ge
    toClientsLobbyMode toLobby; //Data som skickas i lobbyl�ge
    gameClient players[4];      //Data f�r de anslutna klienterna
    gameClient hsToPlayers[10]; //De tio h�gsta highscoreposterna - Inneh�ller data om klient
    sendHS hsData;              //Struktur som skickar de tio highscore-posterna till klienterna

    struct unit *units[MAX_NUMBER_OF_UNITS];    //Alla enheter

    int numberOfUnits;          //Antalet enheter p� spelplanen
    int eachPlayerUnits;        //Antalet enheter f�r varje spelare

	int quit, quit2, len;       //Avslutningsvariabler quit = lobbyl�ge, quit2 = spell�ge
	int gameover = 0;

	int clientsConnected = 0;   //Antalet anslutna klienter
	int full = 0;               //Flagga - 1 pom servern har 4 anslutna klienter
	int result;                 //Resultat f�r SDLNet_TCP_Recv

    int playerUnitCount[4];     //Antalet enheter varje spelare har
    int playerID = 1;           //Det nummer som identifierar varje spelare
    int playerTurn = 1;         //Anger vem som b�rjar allra f�rst
    int playerDone = 0;         //Indikerar att en spelare �r klar med sin tur

    int playerDead = 0;         //Blir 1 om alla enheter hos en spelare �r d�da
	int z,i;                    //Generiska r�knare
	int thisUnit;               //Index - anger enhet i arrayen units[ ]. G�ller unit som ska flyttas
	int defender;               //Index - som ovan, fast f�r f�rsvarare i strid.

    //Initiera strukturer som ska skickas
	fromLobby.startGame = 0;
	fromLobby.iDisconnectFromLobby = 0;
	fromLobby.map = 0;

	toLobby.playerNo = 1;
    toLobby.playerTurn = 0;
    toLobby.okToStart = 0;
    toLobby.playerQuant = 0;

    sendData.gameOver = 0;
    sendData.disconnectedPlayer = 0;
    sendData.nextPlayerTurn = 0;

    recData.unitID = 0;
    recData.direction = 0;
    recData.player = 0;
    recData.iDisconnect = 0;
    recData.endMyTurn = 0;
    recData.skipThisUnit = 0;

	SDL_Thread *chatThread=NULL;    //Chattr�den
	int noDataToThread;

    //Dirigera om stdout/stderr till konsolf�nstret (i windows)
    //freopen( "CON", "wt", stdout );
    //freopen( "CON", "wt", stderr );

    srand(time(0));

    //Initiera N�tverk
	if (SDLNet_Init() < 0)
	{
		fprintf(stderr, "SDLNet_Init: %s\n", SDLNet_GetError());
		exit(EXIT_FAILURE);
	}
	//Starta chattr�d
	chatThread=SDL_CreateThread(chat_main, &noDataToThread);

    //Resolvera hostName. NULL g�r att servern lyssnar p� det egna n�tverkskortet
	if (SDLNet_ResolveHost(&ip, NULL, 2000) < 0)
	{
		fprintf(stderr, "SDLNet_ResolveHost: %s\n", SDLNet_GetError());
		exit(EXIT_FAILURE);
	}

	//�ppna anslutning, lyssna p� den egna porten
	if (!(server = SDLNet_TCP_Open(&ip)))
	{
		fprintf(stderr, "SDLNet_TCP_Open: %s\n", SDLNet_GetError());
		exit(EXIT_FAILURE);
	}

	for(i = 0; i < 4; i++){
        playerUnitCount[i] = 0;
    }

    //Allokera socket set
    socketset = SDLNet_AllocSocketSet(NUMSOCKET);
    if(socketset == NULL){
        printf("Socketset couldnt be created\n");
        return -1;
    }
    else{
        printf("Socketset created. No: %d\n", NUMSOCKET);
    }

    //Nollst�ll klientsockets
    for(i = 0; i < NUMSOCKET; i++){
        clients[i] = NULL;
    }
    printf("Sockets initialized\n");

    //L�gg till server-socket till socketsetet
    SDLNet_TCP_AddSocket(socketset, server);

    while(1){

        quit = 0;
        //Lobbyl�ge - H�r tas klienter emot till servern
        while (!quit)
        {
            SDLNet_CheckSockets(socketset, 0);
            //S� l�nge klienten inte �r full
            if(!full){
                if(SDLNet_SocketReady(server)){
                    printf("Incoming Connection...\n");

                    //Acceptera klienten, l�gg in i vektor
                    tempClient = SDLNet_TCP_Accept(server);
                    clients[clientsConnected] = tempClient;
                    SDLNet_TCP_AddSocket(socketset, clients[clientsConnected]);

                    if(SDLNet_TCP_Send(clients[clientsConnected], &toLobby, sizeof(toLobby)) < sizeof(toLobby)){
                        fprintf(stderr, "SDLNet_TCP_Send: %s\n", SDLNet_GetError());
                        exit(EXIT_FAILURE);
                    }


                    SDLNet_TCP_Recv(clients[clientsConnected], &fromLobby, sizeof(fromLobby));

                    strcpy(players[clientsConnected].name,fromLobby.name);

                    strcpy(players[clientsConnected].ip, getIP(clients[clientsConnected]));

                    players[clientsConnected].score = 0;

                    printf("%s har anslutit fr\x86n %s\n",players[clientsConnected].name,players[clientsConnected].ip);

                    clientsConnected++;

                    toLobby.playerQuant = clientsConnected;

                    for(i = 0; i < clientsConnected; i++){
                        toLobby.playerNo = i+1;
                        SDLNet_TCP_Send(clients[i], &toLobby, sizeof(toLobby));
                          printf("Sending playerNo: %d to player %d", toLobby.playerNo, clientsConnected);
                                    toLobby.playerNo++;
                    }

                    if(clientsConnected == 4){
                        full = 1;
                    }
                }
            }

            if(clientsConnected >= 1){
                for(i = 0; i < clientsConnected; i++){
                    if(SDLNet_SocketReady(clients[i])){
                        if(clients[i] != NULL){
                            result = SDLNet_TCP_Recv(clients[i], &fromLobby, sizeof(fromLobby));

                            if(result == -1){

                                printf("Player Disconnected from lobby\n");
                                SDLNet_TCP_DelSocket(socketset, clients[i]);
                                SDLNet_TCP_Close(clients[i]);
                                clients[i] = NULL;
                                rearrangeClients(clients, i, clientsConnected);
                                clientsConnected--;

                                toLobby.playerNo = 1;
                                toLobby.playerTurn =0;
                                toLobby.okToStart = 0;
                                for(i = 0; i < clientsConnected; i++){
                                    SDLNet_TCP_Send(clients[i], &toLobby, sizeof(toLobby));
                                    toLobby.playerNo++;
                                }
                                toLobby.playerNo = clientsConnected+1;
                                break;
                            }
                        }
                    }
                }
            }
            //Sartsignal mottagen
            if(fromLobby.startGame == 1){

                loadMap(map,fromLobby.map+1);
                numberOfUnits = spawnUnits(units, fromLobby.map+1);
                eachPlayerUnits = numberOfUnits/clientsConnected;

                for(i = 0; i < numberOfUnits; i++){
                    combatMapSet(units[i]);
                }

                for(i = 0; i < clientsConnected; i++){
                    playerUnitCount[i] = eachPlayerUnits;
                }

                //Ladda in karta
                fromLobby.startGame = 0;
                quit = 1;
            }

            SDL_Delay(50);
        }

        toLobby.playerNo = 0;
        toLobby.playerTurn = 1;
        toLobby.okToStart = 1;
        toLobby.map = fromLobby.map+1;
        toLobby.playerQuant = clientsConnected;

        for(z = 0; z < clientsConnected; z++){
            if(SDLNet_TCP_Send(clients[z], &toLobby, sizeof(toLobby)) < sizeof(toLobby)){
                    fprintf(stderr, "SDLNet_TCP_Send: %s\n", SDLNet_GetError());
                    exit(EXIT_FAILURE);
            }
        }

        quit2 = 0;
        while (!quit2)
        {

            SDLNet_CheckSockets(socketset, 0);

            //Kontrollera meddelanden fr�n spelaren vars tur det �r nu
            printf("T");
            fflush(stdout);
              if(SDLNet_SocketReady(clients[playerTurn-1])){
                  if(clients[playerTurn-1] != NULL){
                        printf("Incoming Data..\n");
                           result = SDLNet_TCP_Recv(clients[playerTurn-1], &recData, sizeof(recData));
                            printf("Result: %d\n", result);
                            if(result > 0){

                                thisUnit = findUnitWithID(units, recData.unitID, numberOfUnits);
                                sendData.unitID = recData.unitID;

                                printf("Mottaget: uID:%d dir:%d pl:%d iDis:%d end:%d skip:%d \n",recData.unitID, recData.direction, recData.player,recData.iDisconnect,recData.endMyTurn,recData.skipThisUnit);
                                fflush(stdout);
                                //Om spelaren vill skippa en enhet
                                if(recData.skipThisUnit == 1){
                                    printf("A player has chosen to skip a unit\n");
                                    sendData.okToMove = 0;
                                    sendData.combat = 0;
                                    sendData.combatLoser = 0;
                                    units[thisUnit]->movePts = 0;
                                }

                                //Om spelaren v�ljer att avsluta sin tur
                                if(recData.endMyTurn == 1){
                                    printf("A player has chosen to end his turn\n");
                                    sendData.okToMove = 0;
                                    sendData.combat = 0;
                                    sendData.combatLoser = 0;
                                    sendData.nextPlayerTurn = 1;
                                    playerTurn++;
                                    if(playerTurn > clientsConnected)
                                        playerTurn = 1;
                                    resetMovePts(units, numberOfUnits);
                                }
                                else{

                                    //blir noll om spelaren f�rs�ker flytta utanf�r sk�rmen eller till ett hinder
                                    sendData.okToMove = (checkBounds(units[thisUnit], recData.direction) &&
                                                            checkMapBounds(units[thisUnit],recData.direction, map));

                                    if(units[thisUnit]->movePts <= 0){
                                        sendData.okToMove = 0;
                                    }

                                    //Om spelaren flyttade in i en upptagen ruta blir combat 0 om det var en
                                    //enhet som spelaren sj�lv kontrollerar, annars blir den uID:t has den attackerade enheten
                                    if(units[thisUnit]->movePts != 0)
                                        sendData.combat = checkUnitCollision(units[thisUnit], recData.direction);

                                    //Om spelaren f�rs�ker g� in i en tom ruta returneras 1, alla andra rutor
                                    //(de med enheter i) ska hindra enheten fr�n att r�ra p� sig.
                                    if(sendData.combat != 1){
                                        sendData.okToMove = 0;
                                    }
                                    //Om spelaren f�rs�ker g� in i en ruta med en fiende p�
                                    if(sendData.combat != 0 && sendData.combat != 1){
                                        if(units[thisUnit]->movePts != 0)
                                            units[thisUnit]->movePts--;

                                        //Defender �r den soldat som anf�lls
                                        defender = findUnitWithID(units, sendData.combat, numberOfUnits);
                                        //Best�m vem som vann striden
                                        sendData.combatLoser = initCombat(units[thisUnit], units[defender],map);
                                        playerUnitCount[sendData.combatLoser/10-1]--;

                                        units[findUnitWithID(units, sendData.combatLoser,numberOfUnits)]->movePts = 0;

                                        //Om anfallaren f�rlorade
                                        if(units[thisUnit]->uID == sendData.combatLoser){
                                            players[(units[defender]->uID/10)-1].score += 100;
                                        }
                                        //Om f�rsvararen f�rlorade
                                        if(units[defender]->uID == sendData.combatLoser){
                                            players[(units[thisUnit]->uID/10)-1].score += 150;
                                        }
                                        for(i = 0; i < clientsConnected; i++){
                                            printf("Player %d score: %d\n", i+1, players[i].score);
                                        }
                                    }
                                    else{
                                        sendData.combatLoser = 0;
                                    }

                                    //Flytta enheten
                                    if(sendData.okToMove){
                                        sendData.moveDirection = recData.direction;
                                        combatMapClear(units[thisUnit]);
                                        moveUnit(units[thisUnit], recData.direction);
                                        if(units[thisUnit]->movePts != 0)
                                            units[thisUnit]->movePts--;
                                        printf("I IF - sendData.okToMove, units[%d]->movePts = %d\n", thisUnit, units[thisUnit]->movePts);
                                        combatMapSet(units[thisUnit]);
                                    }

                                    //Kontrollera om alla spelarens enheter har g�tt klart
                                    for(i = 0; i < numberOfUnits; i++){
                                        if(units[i]->uID/10 == recData.player){
                                            if((units[i]->movePts <= 0 || units[i]->dead == 1)){
                                                printf("Unit %d movePts: %d\n", i, units[i]->movePts);
                                                playerDone++;
                                                printf("PlayerDone: %d\n", playerDone);
                                            }
                                        }
                                    }
                                    //Om spelaren har g�tt klart med alla enheter
                                    if(playerDone == eachPlayerUnits && recData.iDisconnect == 0){
                                            printf("Player has moved all his units, NextplayerTurn!\n");
                                            printf("PlayerDone: %d\n", playerDone);
                                            sendData.nextPlayerTurn = 1;
                                            //R�kna upp spelarturen
                                            playerTurn++;
                                            if(playerTurn > clientsConnected)
                                                playerTurn = 1;
                                            //�terst�ll enheternas movePts tills n�sta g�ng
                                            resetMovePts(units, numberOfUnits);

                                            playerDone = 0;
                                    }
                                    else{
                                        playerDone = 0;
                                    }

                                    //Kolla om n�gons enheter �r d�da
                                    for(i = 0; i < clientsConnected; i++){
                                        if(playerUnitCount[i] == 0)
                                            playerDead++;
                                    }
                                    //Om alla utom en spelares enheter �r d�da
                                    if(playerDead == clientsConnected-1){
                                        printf("GAME OVER\n");
                                            //Spel slut
                                            for(i = 0; i < clientsConnected; i++){
                                                updateHiScore(players[i]);
                                            }
                                            get10Highest(hsToPlayers);
                                            //Skicka hsToPlayers till alla klienter
                                            for(i = 0; i < 10; i++){
                                                hsData.hs10[i] = hsToPlayers[i];
                                            }
                                            sendData.gameOver = 1;
                                            //Skicka gameOver signal
                                            for(i = 0; i < clientsConnected; i++){
                                                SDLNet_TCP_Send(clients[i], &sendData, sizeof(sendData));
                                            }
                                            //Skicka highScore
                                            for(i = 0; i < clientsConnected; i++){
                                                SDLNet_TCP_Send(clients[i], &hsData, sizeof(hsData));
                                            }
                                            gameover = 1;
                                    }
                                    else{
                                        playerDead = 0;
                                    }

                                }

                                if(!gameover){
                                    for(i = 0; i < clientsConnected; i++){
                                        len = sizeof(sendData);
                                        if (SDLNet_TCP_Send(clients[i], &sendData, len) < len){
                                            printf("Client failed to respond");
                                        }
                                    }
                                }

                                sendData.nextPlayerTurn = 0;

                                SDL_Delay(50);

                            }
                            //Om det var n�got fel p� anslutningen
                            if(result < 0){
                                printf("Disconnecting player\n");
                                SDLNet_TCP_Close(clients[playerTurn-1]);
                                SDLNet_TCP_DelSocket(socketset, clients[playerTurn-1]);
                                clients[playerTurn-1] = NULL;
                                rearrangeClients(clients, playerTurn-1, clientsConnected);
                                clientsConnected--;
                                printf("Player disconencted\n");
                                printf("Connected clients: %d\n", clientsConnected);
                            }
                    }
                }
                //Kolla alla andra om de har kopplat ner sig
                for(i = 0; i < clientsConnected; i++){
                    if(SDLNet_SocketReady(clients[i])){
                        if(clients[i] != NULL){
                            result = SDLNet_TCP_Recv(clients[i], &passiveData, sizeof(passiveData));
                            //Om det var n�got fel p� anslutningen
                            if(result < 0){
                                printf("A watcher has disconnected\n");
                                SDLNet_TCP_Close(clients[i]);
                                SDLNet_TCP_DelSocket(socketset, clients[i]);
                                clients[i] = NULL;
                                rearrangeClients(clients, i, clientsConnected);
                                clientsConnected--;
                                printf("Clients C: %d\n", clientsConnected);
                            }
                        }
                    }
                }
                if(gameover){
                    for(i = 0; i < clientsConnected; i++){
                        SDLNet_TCP_Close(clients[i]);
                        SDLNet_TCP_DelSocket(socketset, clients[i]);
                        clients[i] = NULL;
                    }
                    clientsConnected = 0;
                }

                //Avluta spelet d� det inte �r n�gra fler spelare anslutna
                if(clientsConnected <= 0){
                    quit2 = 1;
                }

                SDL_Delay(50);
            }

            //�terst�ll alla data
            quit2 = 0;
            clientsConnected = 0;
            full = 0;
            playerID = 1;
            playerTurn = 1;
            playerDone = 0;
            gameover = 0;
            playerDead = 0;
            printf("Resetting server data\n");

            for(i = 0; i < numberOfUnits; i++){
                combatMapClear(units[i]);
            }

            quit = 0;

            fflush(stdout);

            toLobby.okToStart = 0;
            toLobby.playerTurn = 0;
            toLobby.playerNo = 1;

            fromLobby.iDisconnectFromLobby = 0;
            fromLobby.map = 0;
            fromLobby.startGame = 0;

            recData.unitID = 0;
            recData.direction = 0;
            recData.player = 0;

            sendData.combat = 0;
            sendData.combatLoser = 0;
            sendData.unitID = 0;
            sendData.moveDirection = 0;
            sendData.nextPlayerTurn = 0;
            sendData.disconnectedPlayer = 0;
            clientsConnected = 0;

            for(i = 0; i < NUMSOCKET; i++){
                clients[i] = NULL;
            }

            for(i = 0; i < numberOfUnits; i++){
                free(units[i]);
            }
            printf("Restarting\n");
            fflush(stdout);
    }

    printf("Shutting down\n");
    SDLNet_TCP_Close(server);
    SDLNet_Quit();

    return 0;
}
