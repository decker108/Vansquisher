/**     Skriven av      **/
/** Alexander Bj�rklund **/
/**     2010-05-17      **/
/**                     **/
/**    Reviderad av    **/
/** Alexander Bj�rklund **/
/**     2010-05-17      **/
/*************************/

#include <string.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include "SDL/SDL.h"
#include "SDL/SDL_net.h"
#include "textformat.h"

char *getIP(TCPsocket sock){
    char *str;
    IPaddress *ip;
    ip = SDLNet_TCP_GetPeerAddress(sock);

    str=formatText("dsdsdsdc",ip->host>>24,".", (ip->host>>16)&0xff,".", (ip->host>>8)&0xff,".", ip->host&0xff, '\0');

    return str;
}

char *formatText(char *format,...)
{
	va_list ap;
	Uint32 len=0;
	static char *str=NULL;
	char *p, *s;
	char c;
	int d;
	unsigned int u;

	if(str)
	{
		free(str);
		str=NULL;
	}
	if(!format)
		return(NULL);
	va_start(ap,format);
	for(p=format; *p; p++)
	{
		switch(*p)
		{
			case 's': // str�ng
				s=va_arg(ap, char*);
				str=(char*)realloc(str,((len+strlen(s)+4)/4)*4);
				sprintf(str+len,"%s",s);
				break;
			case 'c': // char
				c=(char)va_arg(ap, int);
				str=(char*)realloc(str,len+4);
				sprintf(str+len,"%c",c);
				break;
			case 'd': // int
				d=va_arg(ap, int);
				str=(char*)realloc(str,((len+64)/4)*4);
				sprintf(str+len,"%d",d);
				break;
			case 'u': // unsigned int
				u=va_arg(ap, unsigned int);
				str=(char*)realloc(str,((len+64)/4)*4);
				sprintf(str+len,"%u",u);
				break;
		}

		if(str)
			len=strlen(str);
		else
			len=0;
	}
	va_end(ap);
	return(str);
}
