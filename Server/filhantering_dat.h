/*
 * filhantering_dat.h
 *
 * Skapad den 5:e Maj 2010
 * Av: Erik Lundin
 *
 * Till f�r att hantera filer p� servern, d�ribland High-score-lagring.
 *
 */
#ifndef _FILHANTERING_H
#define _FILHANTERING_H

//en struct som inneh�ller spelarinfo (inkl. spelresultat)
typedef struct{
    char ip[16];
    char name[4];
    int score;
}gameClient;

//storleken av en highscore struct
#define HS sizeof(gameClient)
//Uppdaterar hiscorelistan p� servern (hiscores.dat) med nya spelresultat och spelarinfo
void updateHiScore(gameClient get/*char *ip, char *name, int score*/);

//L�gger in de tio h�gsta resultaten i en array
void get10Highest(gameClient hs[]);

//void tempHiScore(char *ip, char *name, int score);

#endif /*_FILHANTERING_H*/
