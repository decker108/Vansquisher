/*
 * filhantering_dat.c
 *
 *  Skapad: 14 apr 2010
 *      Skrivet av: Erik Lundin
 *      Kommenterad av: Erik Lundin
 */
#include <stdio.h>
#include <string.h>
#include "filhantering_dat.h"

//updateHiScore �r funktionen som uppdaterar highscore-listan (hiscores.dat),
//nya resultat med spelar-info skickas med som argument till funktionen
void updateHiScore(gameClient get/*char ip[], char name[], int score*/){

	FILE *file;
	int i, m, pos_same_ip=-1, pos_new_score=-1, dont_write=0, writecount=0;
    gameClient hs[100]; //player;

    //Store the arguments in a highscore struct "player"
    /*strcpy(player.ip,ip);
    strcpy(player.name,name);
    player.score=score;*/

    //Open the highscore file in read mode - �ppna highscorefilen f�r l�sning.
    file = fopen( "hiscores.dat" , "rb" );

    //If opening the file worked, proceed with reading the file. Else: error message. - Om filen �ppnades, b�rja l�sa fr�n den, annars: felmeddelande.
    if(file != NULL)
    {
        //Check if the ip-adress already exists in the file - Kolla om spelarens ip-adress existerar redan
        for( i = 0 ; i < 100; i++)
        {
            //For each lap, store a line from the file in the highscore-array - F�r varje varv, lagra en rad fr�n filen i hs[i]
            fread(&hs[i], HS, 1, file);
            //Control-print-out - Kontrollutskrift:
            printf("%3.d. ip: %s // name: %s // score: %d\n", i+1, hs[i].ip, hs[i].name, hs[i].score);
            //pos_same_ip fungerar som en flagga, �r den inte -1 s� har en position hittats fortsatt s�kning skall inte g�ras
            if(pos_same_ip == -1){
                if(strcmp(hs[i].ip,get.ip)==0)
                {
                    //If the player's new score is better than the player's current highscore: replace the highscore with the new one.
                    if(get.score > hs[i].score)
                    {
                        //copy the position (line) of where the same ip was found in the file
                        pos_same_ip = i;
                    }
                    else
                    {
                        //If the new score wasn't better, no more searching needs to be done.
                        printf("Your previous highscore %d was better / Tidigare resultat var b�ttre", hs[i].score);
                        dont_write = 1;
                        break;
                    }
                }
            }
            //pos_new_score fungerar som en flagga, �r den inte -1 s� har en position hittats
            if(pos_new_score == -1){
                //If the player's score is greater than highscore nr."i", write the new record there.
                if(get.score > hs[i].score)
                {
                    printf("Score blev st�rre, pos_new_score = %d\n",i);
                    pos_new_score = i;
                }
            }
        }
    }
    else // <--- if (file = NULL) do this:
    {
        printf("Error when opening hiscores.dat for reading");
    }
    fclose(file);


        //If the new score isn't high enough for the highscore-list, pos_new_score will still be set to -1. Therefore, nothing needs to be written to the file. Leave it as it was.
        //dont_write is a flag that is set to 1 if the same ip existed, but the already written score was better, therefore nothing needs to be written.
        printf("\n-----------------------------------------------------\n");
        if(pos_new_score > -1 && dont_write == 0)
        {
            //�ppna f�r skrivning till filen
            file = fopen ( "hiscores.dat" , "wb" );
            if(file != NULL){
                for( m = 0; m < 100; m++){
                    //Check if the position for the new score has been reached, if so: write it to the file.
                    if(m == pos_new_score)
                    {

                        fwrite( &get, HS, 1, file );
                        writecount++;
                        printf("%3.d. ip: %s // name: %s // score: %d (**wrote new score**)\n", writecount, get.ip, get.name, get.score);

                        //skriv ut rekordet som fanns p� plats m tidigare, men bara om det inte �r i slutet p� filen
                        //om rekordet som ska skrivas �ver �r det med samma ip-adress, skriv inte omd en raden.
                        if(m!=99 && m!=pos_same_ip){
                            fwrite(&hs[m], HS, 1, file);
                            writecount++;
                            printf("%3.d. ip: %s // name: %s // score: %d (written to file)1\n", writecount, hs[m].ip, hs[m].name, hs[m].score);
                        }
                    }
                    //The previous hiscore of the same ip will not be written to the file, it will be skipped. In other words: it is removed from the highscores file
                    //If pos_same_ip > -1, the same ip was found in the file.
                    //when m = pos_same_ip, don't write to the file. This is to remove the old record of the ip fromt he list.
                    /*if(pos_same_ip > -1)
                    {*/
                        if(m != pos_new_score && m != pos_same_ip)
                        {
                            //den som tidigare l�g sist i listan skrivs inte in i den uppdaterade listan
                            if(m!=99){
                                fwrite(&hs[m], HS, 1, file);
                                writecount++;
                                printf("%3.d. ip: %s // name: %s // score: %d (written to file)2\n", writecount, hs[m].ip, hs[m].name, hs[m].score);

                            }
                        }
                    /*}*/
                    }
                    if(writecount==99){
                        fwrite(&hs[99], HS, 1, file);
                        writecount++;
                        printf("%3.d. ip: %s // name: %s // score: %d (written to file)3\n", writecount, hs[99].ip, hs[99].name, hs[99].score);
                    }
                //st�ng filen
                fclose(file);
            }
            else{ // d.v.s. if (file = NULL)
                printf("Error when opening hiscores.dat for writing");
            }
        }
}

//L�ser in de tio h�gsta resultaten fr�n hiscorelistan.
void get10Highest(gameClient hs2[])
{
    FILE *file2;
    int i;

    file2 = fopen( "hiscores.dat" , "rb" );
    if(file2 != NULL){
        for(i=0; i<10; i++){
            fread(&hs2[i], HS, 1, file2);
        }
    }
    else{
        printf("Error when opening hiscores.dat for reading");
    }
    fclose(file2);
}


//tempHiScore �r till f�r att mellanlagra rekord som ska skrivas in i hiscores.dat
//inte s�kert om den beh�vs, en annan id� �r att lagra resultat i en tempor�r array.
/*void tempHiScore()
{
    FILE *file;
    file = fopen ("stack_hiscores.dat","w+b"); //varje spelare g�r append p� filen, d�remd b�r ingen data f�rsvinna.
    //det g�rs efter varje spelomg�ng
    //Set pointer to the end of the file - S�tt filpekaren i slutet p� filen.
    fseek( file, 0, SEEK_END );

    //ftell now counts the filesize by counting bytes from beginning to the pointer (end) - med ftell r�knas filstorlek ut i bytes.
    filesize = ftell(file);

    //Rewind the file, in other words: set pointer to the beginning of the file. - S�tt filpekaren i b�rjan av filen igen.
    rewind(file);

    while(filesize == 0)
    {
        fwrite();
    }
}
*/
