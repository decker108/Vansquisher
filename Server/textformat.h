/**     Skriven av      **/
/** Alexander Bj�rklund **/
/**     2010-05-17      **/
/**                     **/
/**    Reviderad av    **/
/** Alexander Bj�rklund **/
/**     2010-05-17      **/
/*************************/
#ifndef TEXTFORMAT_H
#define TEXTFORMAT_H

#include <string.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include "SDL/SDL.h"
#include "SDL/SDL_net.h"


//Tar emot en SDL TCPsocket som argument och returnar ip'n
//associerad med den som en str�ng.
char *getIP(TCPsocket sock);

//Tar en str�ng d�r varje bokstav representerar en datortyp
//s f�r str�ng, c f�r char, d f�r int och u f�r unsigned int,
//och sedan ett godtyckligt antal argument av ovanst�ende
//datatyper, lika m�nga och i samma ordning som tecknen i den f�rsta
//str�ngen. funktionen l�gger sedan ihop dessa i en str�ng och returnerar
char *formatText(char *format,...);

#endif
