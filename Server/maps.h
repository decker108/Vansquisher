/*
 * maps.h
 *      Behandlar inl�sning av kartor
 *      Author: Petter L�fgren
 */
#ifndef MAPS_H_
#define MAPS_H_

#include <stdio.h>
#include <stdlib.h>

#define LEVEL_WIDTH 1152
#define LEVEL_HEIGHT 896

#define LEVEL_TILES_COL 18
#define LEVEL_TILES_ROW 14

//Laddar in kartan f�r terr�nger
void loadMap(int map[LEVEL_TILES_COL][LEVEL_TILES_ROW], int whichOne);

#endif
