/*
 * Hanterar f�rflyttning och initiering av
 * kameran.
 * Skriven av: Petter L�fgren.
*/

#ifndef _CAMERA_H_
#define _CAMERA_H_

#include "SDL/SDL.h"

#define SCREEN_W 1024
#define SCREEN_H 768
#define GUI_BAR_H 128

#define LEVEL_WIDTH 1152
#define LEVEL_HEIGHT 896

#define CAM_PXL_MOVE 32

/* Initierar en SDL_Rect inom vilken all grafik ritas ut. Argumenten �r
 * kamerans initiala x och y koordinat. Returnerar En SDL_Rect som
 * fungerar som kamera.
 */
SDL_Rect* initCam(int xPos, int yPos);
/* Ser till att kameran inte g�r utanf�r spelplanen
*/
int keepCamInBounds(SDL_Rect cam, int direction);

/*Flyttar p� kameran. SDL_Rect som kamera och �ndrar
  dennas x/y koordinater beroende p� direction. direction
  kommer fr�n anv�ndar input
*/
void moveCam(SDL_Rect *cam, int direction);

#endif
