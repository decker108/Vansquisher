/*
 * Hanterar olika typer av h�ndelser s�som input
 * och inladdning av highscore
 *
 *  Created on: 7 apr 2010
 *  Skriven av: Petter L�fgren och Ola Rende
 */

#ifndef _EVENTS_H_
#define _EVENTS_H_

#include "SDL/SDL.h"
#include "camera.h"
#include "units.h"

//H�ndelser - Input fr�n tangentbord
#define UP 0
#define DOWN 1
#define RIGHT 2
#define LEFT 3
#define QUIT 10
#define ENTER 15
#define SPACE 18
#define T 14
#define INPUTDONE 16
#define INPUTCONT 17

#define msglen 43

struct texttype formtext[6];  //Text-structar f�r rutorna i lobbyn
struct texttype chattext[18]; //Text-structar f�r chatten i lobbyn
struct texttype gamechattext[5]; //Text-structar f�r chatten i spelet
struct texttype hiscore_txt[10]; //Text-structar f�r hiscore-namn
struct texttype hiscore_num[10]; //Text-structar f�r hiscore-tal

struct texttype { //Struct med inf� om position, inneh�ll och l�ngd f�r GUI-text
    int x_pos;
    int y_pos;
    char str[43];
    int index;
};

//Struktur f�r highscore data
typedef struct{
    char ip[16];
    char name[4];
    int score;
} highScores;

//Mottagen highscore struktur f�rn server
typedef struct {
    highScores hs10[10];
}hsFromServer;

typedef struct{  // Lagrar X- och Y-koordinaterna f�r ett musklick
    int xPos;
    int yPos;
}mouseClick;

SDL_Event event; //h�ndelsevariablen, anv�nds f�r mus- och tgbtryckningar

static int current_menu; //ID f�r nuvarande menyn. 1 = Main menu, 2 = Lobby, 3 = Ingame
static int current_form = 0; //ID f�r nuvarande textinputf�ltet.
static int ip_ok = 0;
static int port_ok = 0;

mouseClick* initMC();

/* Hanterar h�ndelser som har med tangentbordet att g�ra.
 * Returnerar heltal beroende p� input fr�n tangentbordet
 */
int handleKeyInput();

/* Hanterar val av enheter via musklick. Tar emot strukturen
 * f�r en enhet f�r att testa om musen �r �ver n�gon av enheterna.
 * Musklick p� denna enhet f� funktionen att returnera enhetens ID.
 */
int handleMouseInput(mouseClick *mouseC, SDL_Rect *cam, localUnit *lu);

/* Kontrollerar om spelaren har klickat med musen. Returnerar ett om s� �r fallet annars noll
 * Tar emot kameran, musklick-strukturen och grindvariablerna som ser till att musen b�de
 * trycks ned och sl�pps upp.
*/
int checkClick(SDL_Rect *cam, mouseClick *mc, int *mouseClickDown, int *mouseClickUp);

// G�r om knapptryckninar till text i lobby-textf�lten.
// Parametern form visar vilket textf�lt som texten ska hamna i.
void handle_text_lobby(int form, int menu);

void add_chat_row(char* newstr); // L�gger till en str�ng i chattext-structen och f�rskjuter de andra str�ngar ett steg.

int check_ip(char str[20]); // Kontrollerar om den angivna IP-adressen �r giltig eller ej. Returnerar 1 om giltig.

int check_port(char str[7]); // Kontrollerar om den angivna porten �r giltig eller ej. Returnerar 1 om giltig.

void load_hiscores(hsFromServer tenHS); //Laddar in highscore fr�n server i TTF-str�ngar

#endif
