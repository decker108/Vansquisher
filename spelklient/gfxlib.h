/*
Library: gfx.h
Author:  Ola Rende, DP09

Description: Functions and variables for handling graphics in the game.
*/

#ifndef _GFXLIB_H_
#define _GFXLIB_H_

#include "SDL/SDL.h"
#include "SDL/SDL_ttf.h"
#include "units.h"

#define STDROW 14    //standard no of rows for gamemaps
#define STDCOL 18   //standard no of columns for gamemaps
#define STDSQ 64    //standard height/width for tiles (default: 64)
#define NUMTILES 22 //standard number of tiles
#define UNITTILES 12 //standard number of tiles

static const int SCREEN_WIDTH = 1024;
static const int SCREEN_HEIGHT = 768;
static const int SCREEN_BPP = 32;

SDL_Surface *screen;
SDL_Surface *background;
SDL_Surface *unitmap;
SDL_Surface *waiting;
SDL_Surface *text1;
SDL_Surface *text2;
SDL_Surface *hiscore;
SDL_Surface *bsod_background;
SDL_Rect clip[NUMTILES];
SDL_Rect unit_clip[UNITTILES];

//SDL_Event event;

SDL_Rect box[8]; //The attributes of the button

// Blittar k�ll-bilden (enligt x,y koord) p� destinations-ytan.
void apply_surface(int x, int y, SDL_Surface* src, SDL_Surface* dest, SDL_Rect* clip);

// Rita upp bakgrundsterr�ngen enligt matrisen.
// Argument 2 best�mmer vilken matris som ska anv�ndas.
void render_screen(SDL_Rect *cam,int matrix[][STDCOL]);

// Laddar och konverterar en bild till r�tt f�rgformat, returnerar bilden vid framg�ng.
// Argument �r filnamnet som ska anv�ndas.
SDL_Surface *load_image(char* filename);

// Skapar terr�ng clips fr�n laddad bild
// Argument �r clip'ets nummer, antal rutor i x-led, antal rutor i y-led.
void create_clip(int num, int x_offset, int y_offset);

// Ladda in kartfilen i utvalda matrisen.
// Argument �r filnamn och vilken matris (oftast gamearea eller unitpos)
int load_tilepos(char* filename, int matrix[][STDCOL]);

// Laddar tileset/spritemap filen. Endast png-filer!
// Returnerar 1 (framg�ng) eller 0 (misslyckande).
int load_spritemap();

// Best�m koordinater f�r GUI-knapparna.
// Parameter �r vilken meny anv�ndaren �r i.
void mapButtons(int menu);

// Skapar clips fr�n laddad bild
// Argument �r clip'ets nummer, antal rutor i x-led, antal rutor i y-led.
void create_unit_clip(int num, int x_offset, int y_offset);

// H�mtar ett clip-nummer beroende p� vilken enhet som ska ritas ut
// Argument �r en enhet
int get_clip(localUnit* lu);

#endif
