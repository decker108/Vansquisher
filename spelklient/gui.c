//{ Preprocessor directives and globs
//The headers
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "SDL/SDL_ttf.h"
#include "gfxlib.h"
#include <string.h>

#define true 1
#define false 0

//Screen attribs
/*const int SCREEN_WIDTH = 1024;
const int SCREEN_HEIGHT = 768;
const int SCREEN_BPP = 32;
*//*
//The surfaces
SDL_Surface *background = NULL;
SDL_Surface *message = NULL;
SDL_Surface *screen = NULL;
SDL_Surface *GUIsheet = NULL;
*/
//The event structure
SDL_Event event;

//The font that's going to be used
//TTF_Font *font = NULL;

//SDL_Rect clips[4]; //The clip regions of the sprite sheet
//SDL_Rect box[6]; //The attributes of the button
//SDL_Rect* clip; //Set the default sprite

//The color of the font
//SDL_Color textColor = { 0, 0, 0 }; //}
/*
SDL_Surface *load_image( char* filename ) {
    //The image that's loaded
    SDL_Surface* loadedImage = NULL;

    //The optimized surface that will be used
    SDL_Surface* optimizedImage = NULL;

    //Load the image
    loadedImage = IMG_Load( filename );

    //If the image loaded
    if ( loadedImage != NULL ) {
        //Create an optimized surface
        optimizedImage = SDL_DisplayFormat( loadedImage );

        //Free the old surface
        SDL_FreeSurface( loadedImage );

        //If the surface was optimized
        if ( optimizedImage != NULL ) {
            //Color key surface
            SDL_SetColorKey( optimizedImage, SDL_SRCCOLORKEY, SDL_MapRGB( optimizedImage->format, 0, 0xFF, 0xFF ) );
        }
    }

    //Return the optimized surface
    return optimizedImage;
}

int load_files() {
    //Load the button sprite sheet
    GUIsheet = load_image("gui.png");

    //Open the font
    font = TTF_OpenFont("rafika.ttf",28);

    //If there was a problem in loading the button sprite sheet
    if ( GUIsheet == NULL ) {
        return false;
    }

    //If everything loaded fine
    return true;
}

void apply_surface(int x,int y,SDL_Surface* source,SDL_Surface* dest,SDL_Rect* clip) {
    //Holds offsets
    SDL_Rect offset;

    //Get offsets
    offset.x = x;
    offset.y = y;

    //Blit
    SDL_BlitSurface( source, clip, dest, &offset );
}

int init() {
    //Initialize all SDL subsystems
    if ( SDL_Init( SDL_INIT_EVERYTHING ) == -1 ) {
        return false;
    }

    //Initialize SDL_ttf
    if (TTF_Init() == -1) {
        return false;
    }

    //Set up the screen
    screen = SDL_SetVideoMode( SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP, SDL_SWSURFACE );

    //If there was an error in setting up the screen
    if ( screen == NULL ) {
        return false;
    }

    //Set the window caption
    SDL_WM_SetCaption( "GUI Test - tryck pa knapparna", NULL );

    //If everything initialized fine
    return true;
}

void clean_up() {
    //Free the surfaces
    SDL_FreeSurface(GUIsheet);
    SDL_FreeSurface(background);
    SDL_FreeSurface(screen);
    SDL_FreeSurface(message);

    //Close the font
    TTF_CloseFont( font );

    //Quit SDL_ttf
    TTF_Quit();

    //Quit SDL
    SDL_Quit();
}
*/
void mapButtons() {
    box[0].x = 6;   //chatrutan b�rjar i punkt (6,646)
    box[0].y = 646;
    box[0].w = 532; //bredd
    box[0].h = 120; //l�ngd
    //clip = &clips[CLIP_MOUSEOUT]; //default button texture clip (set to "mouseout")

    box[1].x = 622; //placeholder 1
    box[1].y = 671;
    box[1].w = 98;
    box[1].h = 13;

    box[2].x = 622; //placeholder 2
    box[2].y = 715;
    box[2].w = 98;
    box[2].h = 13;

    box[3].x = 755; //skip unit
    box[3].y = 672;
    box[3].w = 62;
    box[3].h = 13;

    box[4].x = 771; //surrender
    box[4].y = 715;
    box[4].w = 75;
    box[4].h = 13;

    box[5].x = 897; //end turn
    box[5].y = 650;
    box[5].w = 112;
    box[5].h = 106;
}

void handle_events() {
    //The mouse offsets
    int x=0,y=0;
    char msgtext[40];

    if (event.type==SDL_MOUSEMOTION) { //anv�nds som default f�r att slippa skr�ptext
        strcpy(msgtext," ");
    } else {
        strcpy(msgtext," ");
    }

    int i;
    for (i=0; i<6; i++) {
        if (event.type==SDL_MOUSEBUTTONDOWN) { //If a mouse button was pressed
            if (event.button.button==SDL_BUTTON_LEFT) { //If left mouse button was pressed
                //Get the mouse offsets
                x = event.button.x;
                y = event.button.y;

                //If the mouse is over the button
                if ((x>box[i].x) && (x<box[i].x+box[i].w) && (y>box[i].y) && (y<box[i].y+box[i].h)) {
                    strcpy(msgtext,"Knapp nedtryckt med musen!");
                    break;
                }
            }
        }
    }

    for (i=0; i<6; i++) {
        //If a mouse button was released
        if (event.type==SDL_MOUSEBUTTONUP) {
            //If the left mouse button was released
            if (event.button.button==SDL_BUTTON_LEFT) {
                //Get the mouse offsets
                x = event.button.x;
                y = event.button.y;

                //If the mouse is over the button
                if ((x>box[i].x) && (x<box[i].x+box[i].w) && (y>box[i].y) && (y<box[i].y+box[i].h)) {
                    strcpy(msgtext," ");
                    break;
                }
            }
        }
    }

    //Get the keystates
    Uint8 *keystates = SDL_GetKeyState( NULL );

    //If up is pressed
    if ( keystates[ SDLK_UP ] ) {
        strcpy(msgtext,"Knapp nedtryckt med tgb!");
    }

    //If down is pressed
    if ( keystates[ SDLK_DOWN ] ) {
        strcpy(msgtext,"Knapp nedtryckt med tgb!");
    }

    //If left is pressed
    if ( keystates[ SDLK_LEFT ] ) {
        strcpy(msgtext,"Knapp nedtryckt med tgb!");
    }

    //If right is pressed
    if ( keystates[ SDLK_RIGHT ] ) {
        strcpy(msgtext,"Knapp nedtryckt med tgb!");
    }

    //skriv text
    message = TTF_RenderText_Solid(font, msgtext, textColor);
}
/*
int main(int argc, char* args[]) {
    //Quit flag
    int quit = false;

    //Initialize
    if ( init() == false ) {
        return 1;
    }

    //Load the files
    if ( load_files() == false ) {
        return 1;
    }

    //Create clips from the sprite sheet
    //set_clips(); //on�digt? hela interfacet blittas som ett clip

    //Map the button coordinates
    mapButtons();

    //While the user hasn't quit
    while (quit==false) {
        //If there's events to handle
        if (SDL_PollEvent(&event)) {
            //Handle button events
            handle_events();

            //If the user has Xed out the window
            if (event.type==SDL_QUIT) {
                //Quit the program
                quit = true;
            }
        }

        //Fill the screen white
        SDL_FillRect(screen,&screen->clip_rect,SDL_MapRGB(screen->format,0xFF,0xFF,0xFF));

        //visa interface
        apply_surface(0,640,GUIsheet,screen,NULL);

        //visa text
        apply_surface(100,150,message,screen,NULL);

        //Update the screen
        if (SDL_Flip(screen)== -1) {
            return 1;
        }
    }

    //Clean up
    clean_up();

    return 0;
}*/
