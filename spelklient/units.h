/*
 * Hanterar initiering av enheter, vart dde ska skapas
 * n�gonstans, och hur de ska f�rflyttas
 *  Skriven av: Petter L�fgren
 */

#ifndef _UNITS_H_
#define _UNITS_H_

//Antalet pixlar enheten flyttas vid varje uppdatering
#define UNIT_SPEED 4

#define UP 0
#define DOWN 1
#define RIGHT 2
#define LEFT 3

//Enhetstyper
#define SOLDIER 1
#define TANK 2
#define COPTER 3

//Beskriver egenskaper f�r lokala enheter
typedef struct{
	int x;
	int y;
	int width;
	int height;
	//Unik identifierare
	int uID;
	//Flagga - �r enheten d�d
	int dead;
	//Vilken typ av enhet som enheten �r
	int type;
} localUnit;

/* Initierar struktur f�r enheter. Tar emot enhetens initiella v�rden
 * f�r x/y -position samt enhetens unika ID.
 */
localUnit* createLocalUnit(int x, int y, int no, int type);

/* Skapar en upps�ttning enheter beroende p� karta som v�ljs.
 * Tar emot en array d�r alla spelares enheter finns, och kartanj som valts
 * Returnerar antalet enheter som skapades
*/
int spawnUnits(localUnit* units[], int map);

/* Flyttar en enhet ett visst antal pixlar. Tar emot strukturen som
 * beskriver enheten och en int som anger vilket h�ll enheten ska g� �t.
*/
void moveLocalUnit(localUnit *lu, int direction);

/* G�r igenom arrayen som inneh�ller alla enheter tills enheten med r�tt uID
 * p�tr�ffas. Tar emot arrayen med enheter, det uID man vill s�ka efter och antalet
 * enheter som finns p� spelplanen. Returnerar indexet f�r enheten med uID id;
*/
int findLocalUnitWithID(localUnit *allUnits[], int id, int noOfUnits);

#endif
