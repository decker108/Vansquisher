#include "gfxlib.h"
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "SDL/SDL_ttf.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Blittar k�ll-bilden (enligt x,y koord) p� destinations-ytan.
void apply_surface(int x, int y, SDL_Surface* src, SDL_Surface* dest, SDL_Rect* clip){
    SDL_Rect offset; //target surface offsets stored here
    offset.x = x;
    offset.y = y;
    SDL_BlitSurface(src,clip,dest,&offset);
}

// Rita upp bakgrundsterr�ngen enligt matrisen.
// Argument 2 best�mmer vilken matris som ska anv�ndas.
void render_screen(SDL_Rect *cam,int matrix[][STDCOL]){
    int row,col;
    for (row=0; row<STDROW; row++) {
        for (col=0; col<STDCOL; col++) {
            apply_surface(col*STDSQ-cam->x,row*STDSQ-cam->y,background,screen,&clip[matrix[row][col]]);
        }
    }
}

// Laddar och konverterar en bild till r�tt f�rgformat, returnerar bilden vid framg�ng.
// Argument �r filnamnet som ska anv�ndas.
SDL_Surface *load_image(char* filename){
    SDL_Surface* tempImg = NULL;    //temporary image
    SDL_Surface* optImg = NULL;     //optimized image

    tempImg = IMG_Load(filename); //load image from file

    if(tempImg!=NULL) {
        optImg = SDL_DisplayFormat(tempImg); //convert surface to the pixelformat and colors of the video framebuffer.
        SDL_FreeSurface(tempImg); //delete temporary surface from memory

        if(optImg!=NULL) {
            SDL_SetColorKey(optImg,SDL_SRCCOLORKEY,SDL_MapRGB(optImg->format,0,0xFF,0xFF));
        } else {
            fprintf(stderr,"Error: Surface couldn't be optimized.");
        }
    } else {
        fprintf(stderr,"Error: Couldn't load image from file, file may not exist.");
    }

    return optImg;
}

// Skapar terr�ng clips fr�n laddad bild
// Argument �r clip'ets nummer, antal rutor i x-led, antal rutor i y-led.
void create_clip(int num, int x_offset, int y_offset){
    int x_coord,y_coord;

    //set pixel coordinates coordinates
    x_coord=x_offset*STDSQ;
    y_coord=y_offset*STDSQ;

    clip[num].x = x_coord;    //x-coordinate as given from index
    clip[num].y = y_coord;    //y-coordinate as given from index
    clip[num].w = STDSQ;      //constant width from predefs
    clip[num].h = STDSQ;      //constant height from predefs
}

// Ladda in kartfilen i utvalda matrisen.
// Argument �r filnamn och vilken matris (oftast gamearea eller unitpos)
int load_tilepos(char* filename, int matrix[][STDCOL]){
    int row,col;

    FILE *tempfile;
    tempfile=fopen(filename,"rt");
    if (tempfile==NULL) {
        return -1;
    }

    for (row=0; row<STDROW; row++) {
        for (col=0; col<STDCOL; col++) {
            fscanf(tempfile,"%d ",&matrix[row][col]);
        }
    }
    fclose(tempfile);
    return 1;
}

// Laddar tileset/spritemap filen. Endast png-filer!
// Returnerar 1 (framg�ng) eller 0 (misslyckande).
int load_spritemap(){
    background = load_image("gfx/map_tiles.png"); //gfx/tileset2.png
    unitmap = load_image("gfx/unit_tileset.png"); //debug, vanligt filnamn= unit_tileset.png
    waiting = load_image("gfx/waiting.png");
    if(background==NULL) {
        return 0;
    } else {
        return 1;
    }
}

// Best�m koordinater f�r GUI-knapparna.
// Parameter �r vilken meny anv�ndaren �r i.
void mapButtons(int menu) {
    if (menu==1) {
        box[0].x = 359; //play (main menu)
        box[0].y = 238;
        box[0].w = 281;
        box[0].h = 103;

        box[1].x = 359; //exit (main menu)
        box[1].y = 528;
        box[1].w = 281;
        box[1].h = 103;

        box[2].x = 359; //credits (main menu)
        box[2].y = 380;
        box[2].w = 281;
        box[2].h = 103;

    } else if (menu==2) {
        box[0].x = 64; //chat-rutan (lobby)
        box[0].y = 656;
        box[0].w = 512;
        box[0].h = 31;

        box[1].x = 640; //ip-rutan (lobby)
        box[1].y = 96;
        box[1].w = 319;
        box[1].h = 31;

        box[2].x = 640; //port-rutan (lobby)
        box[2].y = 160;
        box[2].w = 319;
        box[2].h = 31;

        box[3].x = 640; //namn-rutan (lobby)
        box[3].y = 224;
        box[3].w = 319;
        box[3].h = 31;

        box[4].x = 729; //join-knappen (lobby)
        box[4].y = 276;
        box[4].w = 141;
        box[4].h = 27;

        box[5].x = 704; //select-knappen (lobby)
        box[5].y = 470;
        box[5].w = 62;
        box[5].h = 27;

        box[6].x = 717; //start-knappen (lobby)
        box[6].y = 660;
        box[6].w = 166;
        box[6].h = 27;

        box[7].x = 834; //change-knappen (lobby)
        box[7].y = 470;
        box[7].w = 73;
        box[7].h = 27;
    } else {
        box[0].x = 6;   //chat-window (ingame)
        box[0].y = 646;
        box[0].w = 532;
        box[0].h = 120;

        box[1].x = 639; //no music (ingame)
        box[1].y = 671;
        box[1].w = 69;
        box[1].h = 15;

        box[2].x = 639; //no sound (ingame)
        box[2].y = 714;
        box[2].w = 69;
        box[2].h = 15;

        box[3].x = 755; //skip unit (ingame)
        box[3].y = 672;
        box[3].w = 62;
        box[3].h = 13;

        box[4].x = 771; //surrender (ingame)
        box[4].y = 715;
        box[4].w = 75;
        box[4].h = 13;

        box[5].x = 897; //end turn (ingame)
        box[5].y = 650;
        box[5].w = 112;
        box[5].h = 106;
    }
}

// Skapar clips fr�n laddad bild
// Argument �r clip'ets nummer, antal rutor i x-led, antal rutor i y-led.
void create_unit_clip(int num, int x_offset, int y_offset){
    int x_coord,y_coord;

    //set pixel coordinates coordinates
    x_coord=x_offset*STDSQ;
    y_coord=y_offset*STDSQ;

    unit_clip[num].x = x_coord;    //x-coordinate as given from index
    unit_clip[num].y = y_coord;    //y-coordinate as given from index
    unit_clip[num].w = STDSQ;      //constant width from predefs
    unit_clip[num].h = STDSQ;      //constant height from predefs
}

int get_clip(localUnit*lu) {
    int player,type;
    int num;
    player = (lu->uID)/10;
    type = lu->type;

/*    if (type==1) {
        num=0+player;
    } else if (type==2) {
        num=4+player;
    } else {
        num=8+player;
    }
*/

    if (player==1) {
        if (type==1) {
            num=0;
        } else if (type==2) {
            num=4;
        } else {
            num=8;
        }
    } else if (player==2) {
        if (type==1) {
            num=1;
        } else if (type==2) {
            num=5;
        } else {
            num=9;
        }
    } else if(player == 3){
        if (type==1) {
            num=2;
        } else if (type==2) {
            num=6;
        } else {
            num=10;
        }
    } else if(player == 4){
        if (type==1) {
            num=3;
        } else if (type==2) {
            num=7;
        } else {
            num=11;
        }
    }
    return num;
}
