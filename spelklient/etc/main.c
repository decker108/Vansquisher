/*
 * main.c
 *
 *  Created on: 1 apr 2010
 *      Author: Petter
 */

#include "events.h"
#define SCREEN_WIDTH 680
#define SCREEN_HEIGHT 480
#define SCREEN_BPP 32

struct messageFromServer{
	int messType;
	int okToMove;
	int combat;
	int combatLoser;
	int srcX;
	int srcY;
	int destX;
	int destY;
};

/*
struct messageToServer{
	int unitID;
	int direction;
	int player;
	int messType;
};
*/
SDL_Surface *screen = NULL;
SDL_Surface *man = NULL;

SDL_Rect clip[4];

SDL_Surface* load_image(char* filename);

void apply_surface(int x, int y, SDL_Surface* Source, SDL_Surface* destination, SDL_Rect* clip);

void apply_all(int x, int y);

int init();

int load_files();

void clean_up();

int init();

int main(int argc, char* args[])
{
    SDL_Event event;

	struct localUnit *units[2];

	struct messageToServer toSend;
	struct messageFromServer recieved;

	int quit = 0,z,i,x = 0;
	//Till event.c
	int input = -1;
	int unitSelected = 0;
	int whichUnit = -1;
	int unitInArray;

	int playerNo = 0;

	toSend.player = playerNo;

	//Simulera mottagning av serverData
	recieved.okToMove = 0;
	recieved.combat = 1;
	recieved.combatLoser = 0;
	recieved.destX = 0;
	recieved.destY = 0;
	recieved.srcX = 0;
	recieved.srcY = 0;
	recieved.messType = 0;
	//

    if(init() == -1){
    	return 1;
    }

    if(load_files() == -1)
    {
        return 0;
    }

    SDL_WM_SetCaption("Test", NULL);

    //Skapa tv� lokala enheter
    units[0] = createLocalUnit(64*4,64*2,10);
    units[1] = createLocalUnit(64*6,64*2,20);

	SDL_FillRect(screen, &screen->clip_rect, SDL_MapRGB(screen->format, 0xFF, 0xFF, 0xFF));

	apply_surface(units[0]->x,units[0]->y, man, screen, NULL);
	apply_surface(units[1]->x,units[1]->y, man, screen, NULL);

	SDL_Flip(screen);

	while(!quit){
	    //G� igenom alla enheter
		for(i = 0; i < 2; i++){
		    //whichUnit f�r v�rdet uID fr�n den enhet som har valts
			whichUnit = handleMouseInput(units[i]);
			//Om en enhet valdes
			if(whichUnit != -1){
			    //L�gg till uID i structen som ska skickas till servern
				toSend.unitID = whichUnit;
				//En enhet har valts
				unitSelected = 1;
				//Kolla inga fler enheter d� spelaren bara kan klicka
				//p� en enhet �t g�ngen
				break;
			}
		}

        //Ta in tangentbordstryckningar
		input = handleKeyInput();

		if(unitSelected){
			if(input >= UP && input <= LEFT){
			    //Om spelaren har valt en enhet kan vi
			    //skicka vilken riktning den ska g� �t
				toSend.direction = input;
				//H�r �r structen klar att skickas till servern
				printf("toSend unitID: %d \ntoSend direction: %d \ntoSend playerID: %d\n\n", toSend.unitID, toSend.direction, toSend.player);
				//H�r skickas structen till servern
				recieved.okToMove = 1;
				fflush(stdout);
			}
		}

		if(input != -1){
			printf("Input: %d\n\n", input);
		}
		if(input == QUIT)
			quit = 1;
        //S�tt input till -1 s� att knapptryckningarna inte "ligger kvar"
		input = -1;

		/*H�r tas structen emot fr�n servern



		*/
		if(recieved.okToMove){
			unitInArray = findLocalUnitWithID(units, toSend.unitID);
			printf("unitInArray: %d\n", unitInArray);
			for(x = 0; x < 32; x++){
				SDL_FillRect(screen, &screen->clip_rect, SDL_MapRGB(screen->format, 0xFF, 0xFF, 0xFF));
				moveLocalUnit(units[unitInArray], toSend.direction);
				for(i = 0; i < 2; i++){
					apply_surface(units[i]->x, units[i]->y, man, screen,NULL);

//					apply_surface(units[unitInArray]->x, units[unitInArray]->y, man, screen,NULL);
				}
				SDL_Flip(screen);
				SDL_Delay(50);
				recieved.okToMove = 0;
			}
		}
		SDL_Delay(50);
	}

	free(units[0]);
	free(units[1]);

	//Free the loaded image
	SDL_FreeSurface(man);
	//Quit SDL
	SDL_Quit();

    return 0;
}
int init(){

	//Init SDL
	if(SDL_Init(SDL_INIT_EVERYTHING)==-1){
		return -1;
	}

	screen = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP, SDL_SWSURFACE);

	if(screen == NULL)
	{
		return -1;
	}

	return 0;
}

int load_files()
{
    //load the image
    man = load_image("man.png");

    if(man == NULL)
    {
        return -1;
    }
    return 0;
}

void clean_up()
{
    //Free image
    SDL_FreeSurface(man);

    //Quit SDL
    SDL_Quit();
}

void apply_surface(int x, int y, SDL_Surface* source, SDL_Surface* destination, SDL_Rect* clip)
{
    //Make temporary triangle to hold the offsets
    SDL_Rect offset;

    //Give the offset to the rectangle
    offset.x = x;
    offset.y = y;

    //BLit the surface
    SDL_BlitSurface(source, clip, destination, &offset);
}

SDL_Surface* load_image(char* filename)
{
    //Temporary storage gfor the image thats loaded
    SDL_Surface *loadedImage = NULL;

    //The optimized image that will be used
    SDL_Surface* optimizedImage = NULL;

    //Img_Load can load PNG
    loadedImage = IMG_Load(filename);

    if(loadedImage != NULL)
    {
        //Create an optimized Image - Alpha to get Transperanxcy from PNG images
        optimizedImage = SDL_DisplayFormatAlpha(loadedImage);

        //Free the old Image
        SDL_FreeSurface(loadedImage);
    }
    return optimizedImage;
}
