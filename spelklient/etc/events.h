/*
 * events.h
 *
 *  Created on: 7 apr 2010
 *      Author: Petter
 */

#ifndef EVENTS_H_
#define EVENTS_H_

#include "SDL/SDL.h"
#include "SDL/SDL_image.h"

//H�ndelser - Input fr�n tangentbord
#define UP 0
#define DOWN 1
#define RIGHT 2
#define LEFT 3
#define QUIT 10
#define ENTER 15

//
#define UNIT_SPEED 2

//Beskriver egenskaper f�r lokala enheter
struct localUnit{
	int x;
	int y;
	int width;
	int height;
	int uID;
	int dead;
};

//Struktur som ska skickas till servern vid f�rflyttning av enheter
struct messageToServer{
	int unitID;
	int direction;
	int player;
	int messType;
};



struct messageToServer toSend;

//void pollAllEvents();

/* Hanterar h�ndelser som har med tangentbordet att g�ra.
 * Returnerar heltal beroende p� input fr�n tangentbordet
 */
int handleKeyInput();

/* Hanterar val av enheter via musklick. Tar emot strukturen
 * f�r en enhet f�r att testa om musen �r �ver n�gon av enheterna.
 * Musklick p� denna enhet f� funktionen att returnera enhetens ID.
 */
int handleMouseInput(struct localUnit *lu);

/* Initierar struktur f�r enheter. Tar emot enhetens initiella v�rden
 * f�r x/y -position samt enhetens unika ID.
 */
struct localUnit* createLocalUnit(int x, int y, int no);

void moveLocalUnit(struct localUnit *lu, int direction);

int findLocalUnitWithID(struct localUnit *allUnits[], int id);

#endif /* EVENTS_H_ */
