/*
 * events.c
 *
 *  Created on: 7 apr 2010
 *      Author: Petter
 */
#include "events.h"

SDL_Event event;

//En sp�rr som ser till att spelaren b�de m�ste trycka
//ner och sl�ppa upp musen f�r att v�lja en enhet
int mouseDown = 0;
//int quit = 0;
/*
void pollAllEvents(struct localUnit *units[]){
	int i;
	int input = -1;
	int unitSelected = 0;
	int whichUnit = -1;
	int playerNo = 0;

	for(i = 0; i < 2; i++){
		whichUnit = handleMouseInput(units[i]);
		if(whichUnit != -1){
			toSend.unitID = whichUnit;
			unitSelected = 1;
			break;
		}
	}

	input = handleKeyInput();

	if(unitSelected){
		if(input >= UP && input <= LEFT){
			toSend.direction = input;
			printf("toSend unitID: %d \ntoSend direction: %d \ntoSend playerID: %d\n\n", toSend.unitID, toSend.direction, toSend.player);
			fflush(stdout);
		}
	}
	if(input != -1){
		printf("Input: %d\n\n", input);
	}
	if(input == QUIT)
		quit = 1;
	input = -1;
}*/

struct localUnit* createLocalUnit(int x, int y, int no){

    //Skapa pekare till local en enhet
	struct localUnit *lu;
	//Reservera minne
	lu = (struct localUnit*)calloc(1,sizeof(struct localUnit));

    //S�tt x/y Koordinater
	lu->x = x;
	lu->y = y;

	lu->width = 64;
	lu->height = 64;
    //Enhetens unika ID
	lu->uID = no;

	return lu;
}

int handleKeyInput()
{
	if(SDL_PollEvent(&event)) {
		//If a key was pressed
		if(event.type == SDL_KEYDOWN) {
			//Set the proper message surface
			switch(event.key.keysym.sym) {
			case SDLK_UP:
				return UP; //UP = 0
				break;
			case SDLK_DOWN:
				return DOWN; //DOWN = 1
				break;
			case SDLK_RIGHT:
				return RIGHT; //RIGHT = 2
				break;
			case SDLK_LEFT:   //LEFT = 3
				return LEFT;
				break;
			case SDLK_RETURN:   //ENTER = 15
				return ENTER;
				break;
			default:;
			}
		}
		else if(event.type == SDL_QUIT){
			return QUIT;        //QUIT = 10
		}
	}
	//Vid fel
	return -1;
}

int handleMouseInput(struct localUnit *lu)
{
	//Muspekarens x/y koordinater
	int x = 0, y = 0;

	//Om musknappen inte h�lls ner
	if(mouseDown == 0){
	  if(event.type == SDL_MOUSEBUTTONDOWN) { //Kolla om spelaren trycker ner knappen
		if(event.button.button == SDL_BUTTON_LEFT){
					x = event.button.x;
					y = event.button.y;
					if((x > lu->x ) && (x < lu->x + lu->width ) && (y > lu->y) && (y < lu->y + lu->height)){
						//Om musen befinner sig �ver en enhet
						mouseDown = 1;
					}
		}
	  }
	}
	else if(mouseDown == 1){
		if(event.type == SDL_MOUSEBUTTONUP) {
				if(event.button.button == SDL_BUTTON_LEFT){
							x = event.button.x;
							y = event.button.y;
							if((x > lu->x ) && (x < lu->x + lu->width ) && (y > lu->y) && (y < lu->y + lu->height)){
								mouseDown = 0;
								printf("Unit %d SELECTED\n", lu->uID);
								fflush(stdout);
								return(lu->uID);
							}
				}
			}
	}
	return -1;
}

void moveLocalUnit(struct localUnit *lu, int direction)
{
	switch(direction){
	case UP:
		lu->y -= UNIT_SPEED;
		break;
	case DOWN:
		lu->y += UNIT_SPEED;
		break;
	case RIGHT:
		lu->x += UNIT_SPEED;
		break;
	case LEFT:
		lu->x -= UNIT_SPEED;
		break;
	}
}

int findLocalUnitWithID(struct localUnit *allUnits[], int id)
{
	int i;
	for(i = 0; i < 2; i++){
		if(allUnits[i]->uID == id){
			return i;
		}
	}
	//Om enheten inte kunde hittas
	return -1;
}
