/*
 * events.c
 *
 *  Created on: 7 apr 2010
 *  Skriven av: Petter L�fgren och Ola Rende
 */
#include "events.h"

SDL_Event event2;

int handleKeyInput(int mode){
	if(SDL_PollEvent(&event2)) {
	    if (mode == 1) {
	        if (event2.type==SDL_QUIT) {
                return QUIT;
            }
	        if ((event2.type==SDL_KEYDOWN) && (event2.key.keysym.sym==SDLK_RETURN)) {
                return 16;
            } else {
                handle_text_lobby(6,3);
                return 17;
            }
	    } else {
            if(event2.type == SDL_KEYDOWN) {
                switch(event2.key.keysym.sym) {
                case SDLK_UP:
                    return UP; //UP = 0
                    break;
                case SDLK_DOWN:
                    return DOWN; //DOWN = 1
                    break;
                case SDLK_RIGHT:
                    return RIGHT; //RIGHT = 2
                    break;
                case SDLK_LEFT:   //LEFT = 3
                    return LEFT;
                    break;
                case SDLK_RETURN:   //ENTER = 15
                    return ENTER;
                    break;
                case SDLK_SPACE:    //SPACE = 18
                    return 18;
                    break;
                case SDLK_t:        //'t' = 14
                    return 14;
                    break;
                default:;
                }
            } else if(event2.type == SDL_QUIT){
                return QUIT;        //QUIT = 10
            }
	    }
	}
	//Vid fel
	return -1;
}

int handleMouseInput(mouseClick *mouseC,SDL_Rect *cam, localUnit *lu){
    //Om spelaren klickar i en enhets "hitbox"
    if((mouseC->xPos > lu->x) && (mouseC->xPos < lu->x + lu->width)
        &&(mouseC->yPos > lu->y) && (mouseC->yPos < lu->y + lu->height)){
        return lu->uID;
    }
	return -1;
}

mouseClick* initMC(){
    mouseClick *mc;

    mc = (mouseClick*)calloc(1,sizeof(mouseClick));
    //S�tt x/y koord utanf�r sk�rmen s� att spelaren inte riskerar att klicka p� n�got fr�n b�rjan
    mc->xPos = -1;
    mc->yPos = -1;

    return mc;
}

int checkClick(SDL_Rect *cam, mouseClick *mc, int *mouseClickDown, int *mouseClickUp){

    if(event2.type == SDL_MOUSEBUTTONDOWN){
            if(event2.button.button == SDL_BUTTON_LEFT){
                *mouseClickDown = 1;
            }
    }

    else if(event2.type == SDL_MOUSEBUTTONUP){
        if(event2.button.button == SDL_BUTTON_LEFT){
                *mouseClickUp = 1;
        }
    }
    //Om musen b�de har tryckts ner opch sl�ppts upp
    if(*mouseClickUp == 1 && *mouseClickDown == 1){
        printf("button Up\n");
        if(event2.button.y < SCREEN_H - GUI_BAR_H){
            //F�rskjut musens x/y koordinater om han klickar p� sk�rmen
            mc->xPos = event2.button.x+cam->x;
            mc->yPos = event2.button.y+cam->y;
        }
        else{
            //Ingen f�rskjutning om han klickar p� GUI-baren
            mc->xPos = event2.button.x;
            mc->yPos = event2.button.y;
        }
        printf("mouse x: %d mouse y: %d\n", mc->xPos, mc->yPos);
        *mouseClickDown = 0;
        *mouseClickUp = 0;
        return 1;
    }
    return 0;
}



// G�r om knapptryckninar till text i lobby-textf�lten.
// Parametern form visar vilket textf�lt som texten ska hamna i.
void handle_text_lobby(int form, int menu) {
    if (menu==2) {
        if (event.type == SDL_KEYDOWN) { //Om en knapp tryckts ned
            if (formtext[form].index <= msglen-1) { //Om str�ngen �r mindre �n maxl�ngd
                //Om knappen �r ett mellanslag
                if (event.key.keysym.unicode==(Uint16)' ') {
                    formtext[form].str[formtext[form].index] = ' ';
                    formtext[form].index++;
                }
                //Om knappen �r en punkt
                else if (event.key.keysym.unicode==(Uint16)'.') {
                    formtext[form].str[formtext[form].index] = '.';
                    formtext[form].index++;
                }
                //Om knappen �r ett snedstreck
                else if (event.key.keysym.unicode==(Uint16)'/') {
                    formtext[form].str[formtext[form].index] = '/';
                    formtext[form].index++;
                }
                //Om input �r ett tecken mellan tecken nr 33 till 167 i ASCII-tabellen...
                else if ((event.key.keysym.unicode>=(Uint16)33) && (event.key.keysym.unicode<=(Uint16)167)) {
                    formtext[form].str[formtext[form].index] = (char)event.key.keysym.unicode;
                    formtext[form].index++;
                }
            }

            //Om backsteg tryckts ned och str�ngen inte �r 0
            if ((event.key.keysym.sym==SDLK_BACKSPACE) && (strlen(formtext[form].str)!=0)) {
                formtext[form].str[formtext[form].index-1]=' ';
                formtext[form].index--;
            }

            formtext[form].str[formtext[form].index+1]='\0'; //l�gg till /0 i slutet
        }
    }
    if (menu==3) {
        //fprintf(stderr,"Inuti handle_text_lobby (form=%d meny=%d) \n",form,menu); //debug
        if (event2.type==SDL_KEYDOWN) {
            if (gamechattext[4].index <= msglen-1) { //Om str�ngen �r mindre �n maxl�ngd
                //Om knappen �r ett mellanslag
                if (event2.key.keysym.unicode==(Uint16)' ') {
                    gamechattext[4].str[gamechattext[4].index] = ' ';
                    gamechattext[4].index++;
                }
                //Om knappen �r en punkt
                else if (event2.key.keysym.unicode==(Uint16)'.') {
                    gamechattext[4].str[gamechattext[4].index] = '.';
                    gamechattext[4].index++;
                }
                //Om knappen �r ett snedstreck
                else if (event2.key.keysym.unicode==(Uint16)'/') {
                    gamechattext[4].str[gamechattext[4].index] = '/';
                    gamechattext[4].index++;
                }
                //Om input �r ett tecken mellan tecken nr 33 till 167 i ASCII-tabellen...
                else if ((event2.key.keysym.unicode>=(Uint16)33) && (event2.key.keysym.unicode<=(Uint16)167)) {
                    gamechattext[4].str[gamechattext[4].index] = (char)event2.key.keysym.unicode;
                    gamechattext[4].index++;
                }
            }

            //Om backsteg tryckts ned och str�ngen inte �r 0
            if ((event2.key.keysym.sym==SDLK_BACKSPACE) && (strlen(gamechattext[form].str)!=0)) {
                gamechattext[4].str[gamechattext[4].index-1]=' ';
                gamechattext[4].index--;
            }

            gamechattext[4].str[gamechattext[4].index+1]='\0'; //l�gg till /0 i slutet
        }
    }
}

// L�gger till en str�ng i chattext-structen och f�rskjuter de andra str�ngar ett steg.
void add_chat_row(char* newstr){
    int i;
        for (i=16; i>=0; i--) {
            //Flyttar str�ngen p� nuvarande raden till raden ovan.
            strcpy(chattext[i+1].str,chattext[i].str);
        }
        strcpy(chattext[0].str,newstr); //Kopierar den nya str�ngen till chatrutan

        for (i=2; i>=0; i--) {
            //Flyttar str�ngen p� nuvarande raden till raden ovan.
            strcpy(gamechattext[i+1].str,gamechattext[i].str);
        }
        strcpy(gamechattext[0].str,newstr); //Kopierar den nya str�ngen till chatrutan
}

int check_ip(char str[20]) {
	int octet1,octet2,octet3,octet4;
	sscanf(str,"%d.%d.%d.%d",&octet1,&octet2,&octet3,&octet4);

	if (octet1>=0 && octet1<255) {
	    if (octet2>=0 && octet2<255) {
	    	if (octet3>=0 && octet3<255) {
	    		if (octet4>=0 && octet4<255) {
	    			return 1;
	    		} else {
	    		    return 0;
	    		}
	    	} else {
                return 0;
            }
	    } else {
            return 0;
        }
	} else {
        return 0;
    }
}

int check_port(char str[7]){
    int num;
    sscanf(str,"%d",&num);

    if (num>0 && num<=65535) {
        return 1;
    } else {
        return 0;
    }
}

void load_hiscores(hsFromServer tenHS){
    //start p� hiscore-del
    char temp[10];
    int i;
    int x=400,y=150;
    for (i=0; i<10; i++) {
    	strcpy(hiscore_txt[i].str, tenHS.hs10[i].name);
        hiscore_txt[i].x_pos=x;
        hiscore_txt[i].y_pos=y;
        sprintf(temp,"%d",tenHS.hs10[i].score);
    	strcpy(hiscore_num[i].str,temp);
    	hiscore_num[i].x_pos=x+150;
        hiscore_num[i].y_pos=y;
        y=y+35;
    }
}
