/*************************/
/**     Skriven av      **/
/** Alexander Bj�rklund **/
/**     2010-05-09      **/
/**                     **/
/**    Reviderad av    **/
/** Alexander Bj�rklund **/
/**     2010-05-17      **/
/*************************/

#ifndef _CHATTHREAD_H_
#define _CHATTHREAD_H_

#include "events.h"

int net_thread_main(void *data);

char *getMsg(TCPsocket sock, char **buf);

int putMsg(TCPsocket sock, char *buf);

#endif
