#include "units.h"

#include <stdlib.h>
#include <stdio.h>

localUnit* createLocalUnit(int x, int y, int type,int no){

    //Skapa pekare till local en enhet
	localUnit *lu;
	//Reservera minne
	lu = (localUnit*)calloc(1,sizeof(localUnit));

    //S�tt x/y Koordinater
	lu->x = x;
	lu->y = y;

	lu->width = 64;
	lu->height = 64;
    //Enhetens unika ID
	lu->uID = no;

	lu->type = type;

	lu->dead = 0;

	return lu;
}

int spawnUnits(localUnit* units[], int map)
{
    int numberOfUnits;
    switch(map){
        case 1:
            units[0] = createLocalUnit(64*1,64*3, SOLDIER,10);
            units[1] = createLocalUnit(64*2,64*3, SOLDIER,11);
            units[2] = createLocalUnit(64*2,64*4, SOLDIER,12);
            units[3] = createLocalUnit(64*0,64*5, TANK,13);
            units[4] = createLocalUnit(64*3,64*5, TANK,14);
            units[5] = createLocalUnit(64*4,64*4, COPTER, 15);

            units[6] = createLocalUnit(64*14,64*10, SOLDIER,20);
            units[7] = createLocalUnit(64*13,64*10, SOLDIER,21);
            units[8] = createLocalUnit(64*13,64*9, SOLDIER,22);
            units[9] = createLocalUnit(64*12,64*8, TANK,23);
            units[10] = createLocalUnit(64*12,64*7, TANK,24);
            units[11] = createLocalUnit(64*15,64*8, COPTER,25);

            numberOfUnits = 12;
            break;
        case 2:
            units[0] = createLocalUnit(64*1,64*3, SOLDIER,10);
            units[1] = createLocalUnit(64*2,64*3, SOLDIER,11);
            units[2] = createLocalUnit(64*3,64*4, SOLDIER,12);
            units[3] = createLocalUnit(64*0,64*5, SOLDIER,13);
            units[4] = createLocalUnit(64*4,64*5, TANK,14);
            units[5] = createLocalUnit(64*4,64*3, TANK,15);
            units[6] = createLocalUnit(64*4,64*4, COPTER, 16);

            units[7] = createLocalUnit(64*14,64*10, SOLDIER,20);
            units[8] = createLocalUnit(64*13,64*10, SOLDIER,21);
            units[9] = createLocalUnit(64*12,64*9, SOLDIER,22);
            units[10] = createLocalUnit(64*13,64*8, SOLDIER,23);
            units[11] = createLocalUnit(64*12,64*8, TANK,24);
            units[12] = createLocalUnit(64*12,64*7, TANK,25);
            units[13] = createLocalUnit(64*15,64*8, COPTER,26);

            units[14] = createLocalUnit(64*2,64*10, SOLDIER,30);
            units[15] = createLocalUnit(64*1,64*10, SOLDIER,31);
            units[16] = createLocalUnit(64*3,64*9, SOLDIER,32);
            units[17] = createLocalUnit(64*4,64*9, SOLDIER,33);
            units[18] = createLocalUnit(64*0,64*9, TANK,34);
            units[19] = createLocalUnit(64*3,64*8, TANK,35);
            units[20] = createLocalUnit(64*5,64*9, COPTER,36);

            numberOfUnits = 21;
            break;
        case 3:
            units[0] = createLocalUnit(64*1,64*3, SOLDIER,10);
            units[1] = createLocalUnit(64*2,64*3, SOLDIER,11);
            units[2] = createLocalUnit(64*3,64*4, SOLDIER,12);
            units[3] = createLocalUnit(64*0,64*5, SOLDIER,13);
            units[4] = createLocalUnit(64*4,64*5, TANK,14);
            units[5] = createLocalUnit(64*4,64*3, TANK,15);
            units[6] = createLocalUnit(64*4,64*4, COPTER, 16);

            units[7] = createLocalUnit(64*14,64*10, SOLDIER,20);
            units[8] = createLocalUnit(64*13,64*10, SOLDIER,21);
            units[9] = createLocalUnit(64*13,64*9, SOLDIER,22);
            units[10] = createLocalUnit(64*13,64*8, SOLDIER,23);
            units[11] = createLocalUnit(64*12,64*8, TANK,24);
            units[12] = createLocalUnit(64*13,64*7, TANK,25);
            units[13] = createLocalUnit(64*15,64*8, COPTER,26);

            units[14] = createLocalUnit(64*2,64*10, SOLDIER,30);
            units[15] = createLocalUnit(64*1,64*10, SOLDIER,31);
            units[16] = createLocalUnit(64*3,64*9, SOLDIER,32);
            units[17] = createLocalUnit(64*4,64*9, SOLDIER,33);
            units[18] = createLocalUnit(64*0,64*8, TANK,34);
            units[19] = createLocalUnit(64*3,64*8, TANK,35);
            units[20] = createLocalUnit(64*4,64*8, COPTER,36);

            units[21] = createLocalUnit(64*14,64*0, SOLDIER,40);
            units[22] = createLocalUnit(64*13,64*1, SOLDIER,41);
            units[23] = createLocalUnit(64*13,64*0, SOLDIER,42);
            units[24] = createLocalUnit(64*13,64*2, SOLDIER,43);
            units[25] = createLocalUnit(64*12,64*3, TANK,44);
            units[26] = createLocalUnit(64*12,64*1, TANK,45);
            units[27] = createLocalUnit(64*15,64*0, COPTER,46);

            numberOfUnits = 28;
            break;

        case 4:
            break;
    }
    printf("CREATED UNITS\n");
    return numberOfUnits;
}

void moveLocalUnit(localUnit *lu, int direction){
	switch(direction){
	case UP:
		lu->y -= UNIT_SPEED;
		break;
	case DOWN:
		lu->y += UNIT_SPEED;
		break;
	case RIGHT:
		lu->x += UNIT_SPEED;
		break;
	case LEFT:
		lu->x -= UNIT_SPEED;
		break;
	}
}

int findLocalUnitWithID(localUnit *allUnits[], int id, int noOfUnits)
{
	int i;
	for(i = 0; i < noOfUnits; i++){
		if(allUnits[i]->uID == id){
			return i;
		}
	}
	//Om enheten inte kunde hittas
	return -1;
}
