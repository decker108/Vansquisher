/*
 * Skriven av: Petter L�fgren.
*/

#include "camera.h"
#include "SDL/SDL.h"

SDL_Rect *initCam(int xPos, int yPos)
{
    SDL_Rect *c;
    c = (SDL_Rect*)calloc(1,sizeof(SDL_Rect));

    c->x = xPos;
    c->y = yPos;
    //Kamerans bredd och h�jd
    c->w = 800;
    c->h = 600;

    return c;
}

int keepCamInBounds(SDL_Rect cam, int direction)
{
    switch(direction){
        //Upp
        case 0:
        if(cam.y-CAM_PXL_MOVE < 0)
			return 0;
		break;
		//Ner
		case 1:
		 if(cam.y+SCREEN_H+CAM_PXL_MOVE > LEVEL_HEIGHT+GUI_BAR_H)
            return 0;
        break;
        //H�ger
        case 2:
        if(cam.x+SCREEN_W+CAM_PXL_MOVE > LEVEL_WIDTH)
            return 0;
        break;
        //V�nster
        case 3:
        if(cam.x-CAM_PXL_MOVE < 0)
            return 0;
        break;
    }
    return 1;
}

void moveCam(SDL_Rect *cam, int direction)
{
    switch(direction){
            //Upp
			case 0:
                if(keepCamInBounds(*cam,direction))
                    cam->y -= CAM_PXL_MOVE;
                break;
            //Ner
            case 1:
            if(keepCamInBounds(*cam,direction))
                    cam->y += CAM_PXL_MOVE;
                break;
            case 2:
            //H�ger
            if(keepCamInBounds(*cam,direction))
                    cam->x += CAM_PXL_MOVE;
                break;
            case 3:
            //V�nster
            if(keepCamInBounds(*cam,direction))
                    cam->x -= CAM_PXL_MOVE;
                break;
            default:;
    }
}
