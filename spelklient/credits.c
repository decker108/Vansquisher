/*

Credits - Visar en lista p� vem som har ansvarat f�r vad i spelet.
Skapad av: Erik Lundin DP09
Datum: 20 maj 2010 (20100520)

*/
#include "credits.h"

void getMessages(messages_data msg_info[])
{
    int i;
    strcpy(msg_info[0].text , "- - CREDITS - -");
    strcpy(msg_info[1].text , "  - DESIGN -  ");
    strcpy(msg_info[2].text , "Alex Bj�rklund R.I.");
    strcpy(msg_info[3].text , "Erik Lundin");
    strcpy(msg_info[4].text , "Petter L�fgren");
    strcpy(msg_info[5].text , "Joakim Megert");
    strcpy(msg_info[6].text , "Ola Rende");
    strcpy(msg_info[7].text , "- LEAD PROGRAMMER -");
    strcpy(msg_info[8].text , "Petter L�fgren");
    strcpy(msg_info[9].text , "- LEAD GRAPHICS PROGRAMMER -");
    strcpy(msg_info[10].text , "Ola Rende");
    strcpy(msg_info[11].text , "- LEAD NETWORK PROGRAMMER - ");
    strcpy(msg_info[12].text , "Alex Bj�rklund R.I. ");
    strcpy(msg_info[13].text , "- LEAD FILE SYSTEM PROGRAMMER -");
    strcpy(msg_info[14].text , "Erik Lundin ");
    strcpy(msg_info[15].text , "- LEAD SOUND PROGRAMMERS -");
    strcpy(msg_info[16].text , "Erik Lundin");
    strcpy(msg_info[17].text , "Joakim Megert");
    strcpy(msg_info[18].text , "- LEAD ARTIST -");
    strcpy(msg_info[19].text , "Joakim Megert");
    strcpy(msg_info[20].text , "- ARTISTS -");
    strcpy(msg_info[21].text , "Ola Rende");
    strcpy(msg_info[22].text , "Erik Lundin");
    strcpy(msg_info[23].text , "Alex Bj�rklund R.I.");
    strcpy(msg_info[24].text , "- QUALITY ASSURANCE -");
    strcpy(msg_info[25].text , "Alex Bj�rklund R.I.");
    strcpy(msg_info[26].text , "Erik Lundin");
    strcpy(msg_info[27].text , "Petter L�fgren");
    strcpy(msg_info[28].text , "Joakim Megert");
    strcpy(msg_info[29].text , "Ola Rende");
    strcpy(msg_info[30].text , "- TACK TILL (THANKS TO)- ");
    strcpy(msg_info[31].text , "Lazy Foo' Productions (www.lazyfoo.net)");
    strcpy(msg_info[32].text , "Johan Rende");

    /*msg_info[0].y_offset=1100;
    msg_info[1].y_offset=1020;
    msg_info[2].y_offset=960;
    msg_info[3].y_offset=940;
    msg_info[4].y_offset=920;
    msg_info[5].y_offset=900;
    msg_info[6].y_offset=880;
    msg_info[7].y_offset=860;
    msg_info[8].y_offset=800;
    msg_info[9].y_offset=780;
    msg_info[10].y_offset=720;
    msg_info[11].y_offset=700;
    msg_info[12].y_offset=640;
    msg_info[13].y_offset=620;
    msg_info[14].y_offset=560;
    msg_info[15].y_offset=540;
    msg_info[16].y_offset=480;
    msg_info[17].y_offset=460;
    msg_info[18].y_offset=440;
    msg_info[19].y_offset=380;
    msg_info[20].y_offset=360;
    msg_info[21].y_offset=300;
    msg_info[22].y_offset=280;
    msg_info[23].y_offset=260;
    msg_info[24].y_offset=240;
    msg_info[25].y_offset=180;
    msg_info[26].y_offset=160;
    msg_info[27].y_offset=140;
    msg_info[28].y_offset=120;
    msg_info[29].y_offset=100;
    msg_info[30].y_offset=80;
    msg_info[31].y_offset=20;
    msg_info[32].y_offset=0;*/

    msg_info[0].y_offset=0;
    for(i=1;i<33;i++){
        /*
        //Om inte rubrik
        if(i==0||i==1||i==7||i==9||i==11||i==13||i==15||i==18||i==20||i==24||i==30){
            msg_info[i].y_offset = offsetCount+40;
            offsetCount += msg_info[i].y_offset;
        }
        else{
            msg_info[i].y_offset = offsetCount+20;
            offsetCount += msg_info[i].y_offset;
        }
        */

        msg_info[i].y_offset = 20*i;
        if(i==1||i==7||i==9||i==11||i==13||i==15||i==18||i==20||i==24||i==30)
            msg_info[i].y_offset += 5;
    }
}
