/*

Skapad av Erik Lundin
Datum: 21 maj 2010

Funktioner f�r att visa "credits"
Credits brukar finnas med i spel f�r att visa vilka som utvecklade spelet och vem som had ansvar f�r olika delar.

*/

#ifndef _CREDITS_H
#define _CREDITS_H

#include <stdio.h>
#include <string.h>
//#include "SDL/SDL.h"
//#include "SDL/SDL_image.h"
//#include "SDL/SDL_mixer.h"
//#include "SDL/SDL_ttf.h"

#define fail 0
#define success 1
#define fast 10
#define normal 30
#define slow 50

typedef struct{
    int y_offset;
    char text[100];
}messages_data;

void getMessages(messages_data msg_info[]);

#endif /*_CREDITS_H*/
