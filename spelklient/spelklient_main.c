/*
 * Huvudfil f�r spelklient. Implementerar n�tverkskommunikation
 * grafikutritning, h�ndelse och enhetshantering
 * Skriven av Petter L�fgren, Ola Rende och Erik Lundin
*/

#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "SDL/SDL_net.h"
#include "SDL/SDL_ttf.h"
#include "SDL/SDL_thread.h"
#include "SDL/SDL_mixer.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "gfxlib.h"
#include "events.h"
#include "camera.h"
#include "chatthread.h"
#include "units.h"
#include "credits.h"

#define FRAMES_PER_SECOND 30
#define true 1
#define false 0
#define msglen 43

int gamearea[STDROW][STDCOL]; //Gamearea best�mmer bakgrundsbildens tiles
int unitpos[STDROW][STDCOL]; //Unitpos best�mmer var spelpj�ser ska placeras

SDL_Surface *message = NULL;
SDL_Surface *message2 = NULL;
SDL_Surface *GUIsheet = NULL;
SDL_Surface *mainmenu = NULL;
SDL_Surface *lobby = NULL;
SDL_Surface *text;
TTF_Font *lobby_font = NULL;
TTF_Font *ingame_font = NULL;
TTF_Font *font = NULL;
SDL_Color textColor = { 0, 0, 0 };
SDL_Color textColorBlack = { 0x00, 0x00, 0x00 };
SDL_Color textColorWhite = { 0xFF, 0xFF, 0xFF };

//Bakgrundsmusiken
Mix_Music *music = NULL;

//Ljudeffekt n�r hastigheten p� credits �ndras
Mix_Chunk *roll_fast = NULL;
Mix_Chunk *roll_slow = NULL;

//int index = 0;
char str[15]={""};
SDL_Surface *text;

//Denna str�ng-matris inneh�ller filnamn som anv�nds vid byte av kartor i lobbyn
char bg_maps[4][20]={"map1.txt","map2.txt","map3.txt"};

//F�rflyttningsmeddelande fr�n klient till server
typedef struct{
    //Enhetens ID
	int unitID;
	//H�llet den ska flyttaws ��t
	int direction;
	//Vilken spelare
	int player;
	//Flagga f�r nedkoppling
	int iDisconnect;
	//Flagga f�r att avlsuta spelarens tur
	int endMyTurn;
	//Hoppa �ver den h�r enheten
	int skipThisUnit;
}messageToServer;

//Allm�n meddelandestruktur f�r server-till-klient data
typedef struct{
    //Ett om f�rflyttning giltig
	int okToMove;
	//Om strid intr�ffade = 1
	int combat;
	//Den uID f�r den enhet som f�rlorade stridenb
	int combatLoser;
	//ID f�r enhet som f�rflyttar sig
	int unitID;
	//F�rflyttningsriktning
	int moveDirection;
	//Flagga - n�sta spelares tur
	int nextPlayerTurn;
	//Om en spelare har kopplat ner sig
	int disconnectedPlayer;
	//Flagga - ett d� n�gon spelare har vunnit
	int gameOver;
} messageFromServer;

struct toServerFromPassive{
    int iQuit;
};

typedef struct{            //Data fr�n lobby(klient) till server
    //Startsignal till server
    int startGame;
    //Meddela om nedkoppling
    int iDisconnectFromLobby;
    //Vilken karta som ksa spelas
    int map;
    //Mitt namn
    char name[4];
}initData;

typedef struct{ //Data fr�n server till klient i lobbyl�ge
    //Spelarens ID-nummer
    int playerNo;
    //Vem som b�rjar ha turen
    int playerTurn;
    //Startsignal
    int okToStart;
    //Kartan som ska laddas in
    int map;
    //Antalet spelare
    int playerQuant;
}fromServerLobbyMode;

// Initiates the SDL video subsystem and sets up the window.
// Returns 1 on success or 0 on failure.
// Startar SDL's subsystem, skapar ett f�nster och ger det attribut. Inga parametrar.
// Returnerar 1 vid framg�ng och 0 vid misslyckande.
int initgfx();

// St�nger av SDL's subsystem och rensar det anv�nda minnet. St�nger �ven av spelet
void clean_up() ;

// Laddar in GUI-filer och teckensnitt.
// Inga parametrar, returnerar 1 vid framg�ng, annars 0.
int load_gui_files(int opts);

// St�ller in positioner (x,y) f�r texter i lobbyn.
// Inga parametrar, men anv�nder globala structarna formtext och chattext.
void fill_out_forms();

// Konverterar alla lagrade textstr�ngar till grafisk text.
// Inga parametrar, men anv�nder globala structarna formtext och chattext.
void render_text(int menu);

// Hanterar mustryckningar i lobbyn.
// Parametern menu visar vilken meny anv�ndaren befinner sig i.
// Boxnumrering: 0=chat, 1=ip, 2=port, 3=namn, 4=join, 5=select, 6=start, 7=change
void handle_events(int menu, int *current_map, int *input_mode, int *name_OK, int *joinGame, int *startGame);

// Hanterar mustryckningar f�r ingame GUI'n.
// Parametern �r  en strukt d�r koordinater f�r musklick finns
int handle_events_gui(mouseClick *mouseC);

int main(int argc, char* argv[]){
    int pink=0; //Hemligt p�sk�gg
    //Socketvariabler
    IPaddress ip;		// Server address
    TCPsocket sd;		// Socket descriptor

    SDLNet_SocketSet set = NULL;    //Socketset - anv�nds f�r polling

    localUnit *units[40];    //Array med info om alla enheter p� spelplanen

	mouseClick *mouseC;      //Inneh�ller koordinater f�r musklick
	mouseC = initMC();              //Initiera musklicks-struct

    //Datatyper f�r n�tkommunikation
	messageToServer toSend;
	messageFromServer recieved;
//    struct toServerFromPassive toSendPassive;
    initData fromLobbytoServer;
    fromServerLobbyMode fromServerLM;
    hsFromServer recHSList;



  //  toSendPassive.iQuit = 0;

    //Initiera current_menu (global i events.h)
    current_menu = 1;

	int quit = 0;   //Variablen som kontrollerar klientens mainloop
	int i;          //Generisk r�knare

	int playersInGame;  //Anger hur m�nga spelare som �r anslutna till servern

    int current_map = 0;    //Nuvarande karta
    int input_mode = false; //input l�ge - g�r mellan spell�ge och chattl�ge
    int name_OK = false;    //Flagga om spelaren har skrivit in ett giltigt namn
    int joinGame = 0;       //Flagga - spelaren har tryckt p� joingame
    int startGame = 0;      //Flagga - spelaren har tryckt p� startGame

    int buttonPressed;      //Anger vilken knapp i in-game GUI:t som har tryckts ner
    int mouseClickDown = 0; //Sp�rr/Grind f�r att kontrollera att musen har tryckts ned
    int mouseClickUp = 0;   //Sp�rr/Grind f�r att kontrollera att musen har sl�ppts upp
    int mouseClicked;       //Blir �tt om b�da ovanst�ende �r ett

    int clipNo;             //Det clip som visar vilken enhet som ska ritas ut

    char mapName[9] = "map";    //Den karta som ska laddas in - blir fullst�ndig d� spelarna startar spelet
    char mapNo[2];              //Str�ng som anger vilken karta som ska spelas - konkatineras till mapname

	int numberOfUnits;      //Totalt antal enheter i spelomg�ngen
	//Till event.c
	int input = -1;         //Tal som anger vilken knapp som har tryckts ner
//	int stringInput = 0;
	int unitSelected = 0;   //Flagga - Anger att en enhet har valts
	int unitIsMoving = 0;   //Flagga - Anger om en enhet �r i f�rflyttning
	int whichUnit = -1;
	int unitInArray;        //Index f�r enhet i arrayen units[ ]
	int deadUnit;           //Index f�r enhet i arrayen units[ ] - d� enheten �r d�d
	int input_mode2 = 0;    //Inputl�ge f�r spell�ge
	//int ip_ok = 0;
	//int port_ok = 0;

	int numready;           //Hr mycket aktivitet som finns p� socket setet
	int cSuccess = 1;       //Connect Success, 1 om spelaren lyckats ansluta till servern

//	int skipThesePlayers[4] = {0};

    int startTicks;

    SDL_Rect *camera;       //SDL rektangel. Inom denna ritas allt ut.

    int messageLength;      //L�ngd p� meddelande som skickas

	int playerNo;           //Spelarens ID
	int playerTurn;         //Vem b�rjar
    int inlobby=1;          //Flagga - 1 om spelaren �r i lobbyn
    int connected = 0;      //Flagga 1 om spelaren �r ansluten till servern.

    //Variabler f�r credits
    int textPos_y_int=1600,  roll_speed=normal;
    messages_data msg_info[33];

    //Initiera n�tverksfunktioner
	if (SDLNet_Init() < 0)
	{
		fprintf(stderr, "SDLNet_Init: %s\n", SDLNet_GetError());
		exit(EXIT_FAILURE);
	}

	set = SDLNet_AllocSocketSet(1);
	if(set == NULL){
        printf("Error Allocating Sockets");
	}

    //Initiering av grafik och ljud
    if (initgfx()!=1) {
        fprintf(stderr,"Error: Couldn't init graphics and sound");
        exit(-1);
    }

    //Ladda in tileset-filen
    if(load_spritemap()==0) {
        return 1;
    }

    if(1<argc){
        //Hemligt p�sk�gg
        if(!strcasecmp(argv[1], "/PINK")){
            pink=1;
        }
    }

    //Ladda in interface-bilderna
    if (load_gui_files(pink)==0) {
        return 1;
    }

    //Skapa terr�ng-urklipp (sprites eller clips) ur tileset-filen
    int num=0,row,col;
    for (row=0; row<5; row++) {
        for (col=0; col<5; col++) {
            if (num==21){
                break;
            }
            create_clip(num++,col,row);
        }
    }

    //Skapa enhets-urklipp
    int num2=0;
    for (row=0; row<3; row++) {
        for (col=0; col<4; col++) {
            if (num2==12){
                break;
            }
            create_unit_clip(num2,col,row);
            num2++;
        }
    }

    SDL_EnableKeyRepeat(50, 50);    //Sl�r p� polling f�r input

   // printf("Skapa kamera");
    camera = initCam(64, 64);       //Initiera kamera, xPos och yPos
 //   printf("Kamera Skapad");

    //Initiering av kommunikationsdatatyper
    fromServerLM.playerNo = 0;
    fromServerLM.okToStart = 0;
    fromServerLM.playerTurn = 0;
    fromServerLM.playerQuant = 0;

    fromLobbytoServer.startGame = 0;
    fromLobbytoServer.iDisconnectFromLobby = 0;
    fromLobbytoServer.map = 0;
    strcpy(fromLobbytoServer.name, "\0");

    toSend.iDisconnect = 0;
    toSend.endMyTurn = 0;
    toSend.skipThisUnit = 0;
	toSend.player = playerNo;

    recieved.okToMove = 0;
    recieved.combat = 0;
    recieved.combatLoser = 0;
    recieved.unitID = 0;
    recieved.moveDirection = 0;
    recieved.nextPlayerTurn = 0;
    recieved.disconnectedPlayer = 0;
    recieved.gameOver = 0;

//    render_screen(unitpos); //F�R BARA G�RAS EN G�NG

    //Chatn�tverksvariabler
    TCPsocket chatsock;         //Filehandle f�r socket till chatten
    Uint16 chatport = 5666;     //Chattens port
    IPaddress chatip;           //IP/Host info
    SDL_Thread *net_thread=NULL; //Chatt tr�den

    //St�ll in koordinater p� huvudmenyns knappar.
    mapButtons(current_menu);

    //V�lj en default karta
    strcpy(formtext[5].str,bg_maps[current_map]);

    while(quit == 0) {
        while (inlobby==1) { //Sart p� lobby och huvudmeny
            //Om spelaren har tryckt p� "Join Game" och inte redan �r ansluten
            if(joinGame == 1 && !connected){
                add_chat_row("Connecting...");
                // Resolva den host som ska anslutas till
                if (SDLNet_ResolveHost(&ip, formtext[1].str, atoi(formtext[2].str)) < 0){
                    add_chat_row("Failed to connect to server!");
                    //Connection Success blir 0 d� n�gat av stegen misslyckas, �r annars ett
                    cSuccess = 0;
                }

                // Open a en ansutning
                if (!(sd = SDLNet_TCP_Open(&ip))) {
                    add_chat_row("Failed to create socket!");
                    cSuccess = 0;
                }

                //Resolvera och anslut till chatt-tr�d
                if (SDLNet_ResolveHost(&chatip, formtext[1].str, chatport) < 0) {
                    add_chat_row("Failed to connect to chat-server!");
                    cSuccess = 0;
                }

                if (!(chatsock = SDLNet_TCP_Open(&chatip))) {
                    add_chat_row("Failed to create chat-socket!");
                    cSuccess = 0;
                }

                //Om anslutningen gick felfritt
                if(cSuccess != 0){
                    //Skapa chattr�d
                    net_thread=SDL_CreateThread(net_thread_main,chatsock);
                    if (!net_thread) {
                        printf("SDL_CreateThread: %s\n",SDL_GetError());
                        SDLNet_Quit();
                        SDL_Quit();
                        exit(9);
                    }

                    while (!net_thread) {
                        SDL_Delay(1);
                    }

                    // Det f�rsta meddelandet till chat servern blir spelarens namn
                    if (!putMsg(chatsock,formtext[3].str)) {
                        add_chat_row("Failed to transmit name to chat-server!");
                    }

                    //L�gg socket i socketset
                    SDLNet_TCP_AddSocket(set, sd);
                    //Ta emot init struktur
                    SDLNet_TCP_Recv(sd, &fromServerLM, sizeof(fromServerLM));
                    playerNo = fromServerLM.playerNo;
                    toSend.player = playerNo;
                    fprintf(stderr,"PlayerNo recieved: %d\n", playerNo);

                    //S�tt ihop struktur (Spelarens namn) och skicka till server
                    fromLobbytoServer.startGame = 0;
                    fromLobbytoServer.iDisconnectFromLobby = 0;
                    fromLobbytoServer.map = 0;
                    strcpy(fromLobbytoServer.name, formtext[3].str);
                    SDLNet_TCP_Send(sd, &fromLobbytoServer, sizeof(fromLobbytoServer));

                    connected = 1;
                    joinGame = 0;
                    add_chat_row("Connection successful!");
                }
                else{
                    add_chat_row("Failed to connect!");
                }
                joinGame = 0;
                cSuccess = 1;
            }
            if(connected){
                //Kontrollera socketset efter data
                numready = SDLNet_CheckSockets(set, 0);

                //Om det finns data i socketsetet
                if(numready){
                    if(SDLNet_SocketReady(sd)){
                    //    printf("Something on socket\n");
                        //Ta emot data
                        SDLNet_TCP_Recv(sd, &fromServerLM, sizeof(fromServerLM));

                        printf("What i Recieved:\n playerNo: %d\n playerTurn: %d\n okToStart: %d\n map: %d\n",
                                fromServerLM.playerNo, fromServerLM.playerTurn, fromServerLM.okToStart, fromServerLM.map);

                        playersInGame = fromServerLM.playerQuant;
                        printf("PLAYER Q: %d\n", playersInGame);

                        if(fromServerLM.okToStart == 1){
                            inlobby = 0;
                            //current_menu = 3 �r "menyn" d� spelet k�rs
                            current_menu = 3;
                            //S�tt ut GUI knappar
                            mapButtons(3);
                            playerTurn = fromServerLM.playerTurn;
                            printf("PLAYER Q: %d\n", playersInGame);
                            fprintf(stderr,"PlayerTurn recieved: %d\n", playerTurn);
                            printf("WhatMap: %d\n", fromServerLM.map);

                            //Ladda in kartfilen i gamearea matrisen.
                            strcat(mapName, itoa(fromServerLM.map, mapNo, 10));
                            strcat(mapName, ".txt");

                            if (load_tilepos(mapName,gamearea)!=1) {
                                fprintf(stderr,"Error: Mapfile not found!");
                                exit(-1);
                            }
                            numberOfUnits = spawnUnits(units, fromServerLM.map);
//                            printf("unit 0 X: %d Y: %d\n", units[0]->x, units[0]->y);
                            //G� ut ur lobby d� spelet startats
                            break;
                        }

                        //Nytt spelareID har tagits emot (en spelare disconnectade)
                        if(fromServerLM.playerNo != 0){
                            playerNo = fromServerLM.playerNo;
                            fromServerLM.playerNo = 0;
                            startGame = 0;
                            printf("new playerNo: %d\n", playerNo);
                        }
                    }
                }
            }

            //Om spelare ett trycker p� startgame och �r ansluten
            if(connected == 1 && startGame == 1 && playerNo == 1){
                printf("Trying to start\n");
                //Giltig karta beroende p� antal spelare
                if(current_map == playersInGame-2){
                    fromLobbytoServer.startGame = 1;
                    fromLobbytoServer.iDisconnectFromLobby = 0;
                    fromLobbytoServer.map = current_map;
                    SDLNet_TCP_Send(sd, &fromLobbytoServer, sizeof(fromLobbytoServer));
                    fprintf(stderr,"starTGame Sent\n");
                    fflush(stderr);
                    startGame = 0;
                }
                if((current_map > playersInGame-2)){
                    add_chat_row("Too few players for the current map!");
                }
                if((current_map < playersInGame-2)){
                    add_chat_row("Too many players for the current map!");
                }
                startGame = 0;
            }


            while (SDL_PollEvent(&event)) {
                if (current_menu==1) { //Om anv�ndaren �r i huvudmenyn
                    if (event.type==SDL_MOUSEBUTTONDOWN) { //Om en musknapp tryckts ned
                        if (event.button.button==SDL_BUTTON_LEFT) { //Om det var v�nsterknappen
                            //L�sa av muspekarens koordinater
                            int x=0,y=0;
                            x = event.button.x;
                            y = event.button.y;

                            //Om koordinaterna �verensst�mmer med en GUI-knapp
                            if ((x>box[0].x) && (x<box[0].x+box[0].w) && (y>box[0].y) && (y<box[0].y+box[0].h)) {
                                current_menu = 2; //byt till lobby menyn
                                mapButtons(current_menu); //�ndra knapparnas koordinater
                                fill_out_forms(); //best�m textkoordinater
                                //Mix_PlayMusic(music,-1);
                            }
                            if ((x>box[1].x) && (x<box[1].x+box[1].w) && (y>box[1].y) && (y<box[1].y+box[1].h)) {
                                inlobby = 0; //hoppa ur loopen, st�ng av programmet
                                quit = 1;
                                clean_up();
                            }
                            if ((x>box[2].x) && (x<box[2].x+box[2].w) && (y>box[2].y) && (y<box[2].y+box[2].h)) {
                                current_menu = 5; //byt till credits-menyn
                                getMessages(msg_info);
                                Mix_PlayMusic(music,-1);
                            }
                        }
                    }
                } else if (current_menu==2) { //Om anv�ndaren �r i lobbyn
                    handle_events(current_menu, &current_map, &input_mode, &name_OK, &joinGame, &startGame); //hanterar mustryckningar

                    if (input_mode==true) { //om en textruta markerats...
                        handle_text_lobby(current_form,2); //hanterar tagentbordstryckningar

                        //Om Enter tryckts ned
                        if ((event.type==SDL_KEYDOWN) && (event.key.keysym.sym==SDLK_RETURN)) {
                            if (current_form==0 && connected==1) {
                                putMsg(chatsock,formtext[current_form].str);
                                formtext[current_form].index = 0;
                                strcpy(formtext[current_form].str,"");
                            } else if (current_form==0 && connected==0) {
                                add_chat_row("Error: Not connected to chat server.");
                                formtext[current_form].index = 0;
                                strcpy(formtext[current_form].str,"");
                            }
                            if (current_form==3 && formtext[3].index==3) { //Om namnet �r 3 tecken l�ngt, godk�nn.
                                name_OK = true;
                                strcpy(formtext[4].str,"");
                            } else if (current_form==3 && formtext[3].index!=3) {
                                name_OK = false;
                                strcpy(formtext[4].str,"Name must be 3 letters long!");
                            }
                            ip_ok = check_ip(formtext[1].str);
                            if (current_form==1 && ip_ok==0) {
                                add_chat_row("Error: IP incorrect.");
                            }
                            port_ok = check_port(formtext[2].str);
                            if (current_form==2 && port_ok==0) {
                                add_chat_row("Error: Port incorrect.");
                            }

                            SDL_EnableUNICODE(SDL_DISABLE);
                            input_mode = false;
                            current_form = 4;
                        }
                    }
                } else if (current_menu==5) {
            //If a key was pressed
                        if( event.type == SDL_KEYDOWN ) {
                            switch(event.key.keysym.sym){
                                case SDLK_f:
            //                        Roll credits fast
                                    if( Mix_PlayChannel( -1,roll_fast, 0 ) == -1 ){
                                        return fail;
                                    }
                                    roll_speed=fast;
                                    break;
                                case SDLK_s:
            //                        Roll credits slow
                                    if( Mix_PlayChannel( -1,roll_slow, 0 ) == -1 ){
                                        return fail;
                                    }
                                    roll_speed=slow;
                                    break;
                                case SDLK_p://spela/pausa musiken
            //                        Om ingen musik �r ig�ng (eller pausad)
                                    if( Mix_PlayingMusic() == 0 ){
            //                            Spela bakgrundsmusiken (aybabtu)
                                        if( Mix_PlayMusic( music, -1 ) == -1 ){
                                            return fail;
                                        }
                                    } else{  // Om musiken redan �r ig�ng, spela/pausa
            //                            Om musiken �r pausad
                                        if( Mix_PausedMusic() == 1 ){
                                            Mix_ResumeMusic();
                                        }
                                        else{
                                            Mix_PauseMusic();
                                        }
                                    }
                                    break;
                                case SDLK_q://stoppa musiken
                                    Mix_HaltMusic();
                                    break;
                                case SDLK_ESCAPE:
                                    Mix_HaltMusic();
                                    current_menu=1;
                                    break;
                                default:
                                    break;
                            }
                        }
                        //Om anv�ndaren st�nger av i huvudmenyn
                        if(event.type == SDL_QUIT){
                            clean_up();
                        }

                }

                if (event.type==SDL_QUIT) { //Om anv�ndaren tryckt p� r�da krysset i lobbyn
                    fromLobbytoServer.startGame = 0;
                    fromLobbytoServer.map = 0;
                    fromLobbytoServer.iDisconnectFromLobby = playerNo;
                    if(connected){
                    //    SDLNet_TCP_Send(sd, &fromLobbytoServer, sizeof(fromLobbytoServer));
                     //   SDLNet_TCP_Recv(sd, &fromServerLM, sizeof(fromServerLM));
                    }
                    quit = true; //Avsluta programmet
                    inlobby = 0;
                    clean_up();
                }
            }

            if (current_menu==1) {
                apply_surface(0,0,mainmenu,screen,NULL); //L�gg p� ytan p� sk�rmen
            } else if (current_menu==2) {
                apply_surface(0,0,lobby,screen,NULL); //L�gg p� ytan p� sk�rmen
                render_text(2); //g�r igenom textstr�ngar, renderar alla icke-tomma
            } else if (current_menu==5) {
                textPos_y_int--;
                if (textPos_y_int>0){
                    apply_surface(0, 0, bsod_background, screen, NULL );
                    for(i=0;i<=32;i++){//k�r lika m�nga varv som det finns element i msg_info
                        message = TTF_RenderText_Solid(font, msg_info[i].text, textColorWhite);
                        //Skriv ut texten i mitten av sk�rmen, sk�rmh�jden + y_offset.
                        apply_surface( ( SCREEN_WIDTH - message->w) / 2, textPos_y_int + msg_info[i].y_offset-800, message, screen, NULL );
                    }
                }
            }

            if (SDL_Flip(screen) == -1) { //Uppdatera sk�rmens bild
                return -3;
            }
            //Om spelaren ser credits
            if (current_menu==5) {
                //S�tt in en l�ngre delay
                SDL_Delay(roll_speed);
            }
        } //SLUT P� lobby och huvudsk�rm

        //AKTIVT L�GE, Det �r spelarens tur
        while (quit==0 && playerNo == playerTurn) {

            startTicks = SDL_GetTicks();

            //L�gg p� vit bakgrund
            SDL_FillRect(screen,&screen->clip_rect,SDL_MapRGB(screen->format,0xFF,0xFF,0xFF));

            //L�gg p� bakgrunden
            render_screen(camera,gamearea);

            //Kontrollera om musen har klickat, dess x/y korrdinater sparas i mouseC
            mouseClicked = checkClick(camera, mouseC, &mouseClickDown, &mouseClickUp);

            for(i = 0; i < numberOfUnits; i++){
                if(units[i]->dead == 0){
                    clipNo = get_clip(units[i]);
                    apply_surface(units[i]->x-camera->x, units[i]->y-camera->y, unitmap, screen,&unit_clip[clipNo]);
                }
            }

            //G� igenom alla enheter kontrollera om spelaren har klickat p� n�gon av dem
            for(i = 0; i < numberOfUnits; i++){
                if(units[i]->dead != 1 && !unitIsMoving){
                    //whichUnit f�r v�rdet uID fr�n den enhet som har valts
                    whichUnit = handleMouseInput(mouseC,camera,units[i]);
                    //Om en enhet valdes
                    if(whichUnit != -1 && whichUnit/10 == playerNo){
                        //L�gg till uID i structen som ska skickas till servern
                        toSend.unitID = whichUnit;
                        //En enhet har valts
                        unitSelected = 1;
                        //Kolla inga fler enheter d� spelaren bara kan klicka
                        //p� en enhet �t g�ngen
                        break;
                    }
                }
            }
            //Om spelaren har klickat med musen men inte p� en enhet
            if(whichUnit == -1 && mouseClicked == 1){
                unitSelected = 0;
            }

            //H�r hanteras mus och tagentbordstryckningar.
            buttonPressed = handle_events_gui(mouseC); //input till GUI
            if(buttonPressed && mouseClicked){
                if(buttonPressed == 4){
                    toSend.skipThisUnit = 1;
                    SDLNet_TCP_Send(sd, &toSend, sizeof(toSend));
                    SDLNet_TCP_Recv(sd, &recieved, sizeof(recieved));
                    toSend.skipThisUnit = 0;
                }

                if (buttonPressed == 2) {
                    //no music
                    if( Mix_PlayingMusic() == 0 ){
        //                            Spela bakgrundsmusiken (aybabtu)
                        if( Mix_PlayMusic( music, -1 ) == -1 ){
                            return fail;
                        }
                    } else {  // Om musiken redan �r ig�ng, spela/pausa
    //                            Om musiken �r pausad
                        if( Mix_PausedMusic() == 1 ){
                            Mix_ResumeMusic();
                        }
                        else{
                            Mix_PauseMusic();
                        }
                    }
                }
                if (buttonPressed == 3) {
                    //no sound
                    if( Mix_PlayingMusic() == 0 ){
    //                            Spela bakgrundsmusiken (aybabtu)
                        if( Mix_PlayMusic( music, -1 ) == -1 ){
                            return fail;
                        }
                    } else {  // Om musiken redan �r ig�ng, spela/pausa
    //                            Om musiken �r pausad
                        if( Mix_PausedMusic() == 1 ){
                            Mix_ResumeMusic();
                        }
                        else{
                            Mix_PauseMusic();
                        }
                    }
                }
            }
            //Ta in tangentbordstryckningar
            input = handleKeyInput(input_mode2);

            if (input==T && input_mode2==0) {
                input_mode2 = 1; //Textinput-l�get aktiverat
                SDL_EnableUNICODE(SDL_ENABLE);
                strcpy(gamechattext[4].str,"");
                gamechattext[4].index = 0; //Nollst�ll str�ngens l�ngd
                current_form = 6;
            }
            if (input==INPUTDONE) {
                //Skicka chatstr�ng till server
                putMsg(chatsock,gamechattext[4].str);
                SDL_EnableUNICODE(SDL_DISABLE);
                input = -1;
                input_mode2 = 0;
                gamechattext[4].index = 0;
                strcpy(gamechattext[4].str,"");
            }
            if (input==SPACE && !unitIsMoving) {
                //SPACE avslutar spelarens tur
                toSend.endMyTurn = 1;
                SDLNet_TCP_Send(sd, &toSend, sizeof(toSend));
                SDLNet_TCP_Recv(sd, &recieved, sizeof(recieved));
                toSend.endMyTurn = 0;
                add_chat_row("Turn ended.");
            }

            //Om spelaren st�nger ner
            if(input == QUIT){
                quit = 1;
            }

            //Flytta kameran om spelaren inte har klickat p� en enhet
            if(!unitSelected){
                if(input >= UP && input <= LEFT){
                    moveCam(camera, input);
                }
            }

            //L�t spelaren s�tta ihop strukturer
            if(unitSelected && !unitIsMoving){
                if(input >= UP && input <= LEFT){
                    //Om spelaren har valt en enhet kan vi
                    //skicka vilken riktning den ska g� �t
                    toSend.direction = input;
                    //H�r �r structen klar att skickas till servern

                    //H�r skickas structen till servern

//                    messageLength = sizeof(toSend);
                    if (SDLNet_TCP_Send(sd, &toSend, sizeof(toSend)) < sizeof(toSend))
                    {
                        fprintf(stderr, "SDLNet_TCP_Send: %s\n", SDLNet_GetError());
                        exit(EXIT_FAILURE);
                    }
                    //Slutskickat

                    //Ta emot struktur fr�n server
                    SDLNet_TCP_Recv(sd, &recieved, sizeof(recieved));

                    fflush(stdout);
                }
            }

            //S�tt input till -1 s� att knapptryckningarna inte "ligger kvar"
            if(input != 14){
                input = -1;
            }

            if(recieved.combat != 1 && recieved.combat != 0){
                //Finn index f�r den enhet som f�rlorade
                deadUnit = findLocalUnitWithID(units, recieved.combatLoser, numberOfUnits);
                printf("Combat occured, unit %d\n", deadUnit);
                units[deadUnit]->dead = 1;
                //Division med 10 ger vilken spelare som kontrollerar enheten
                if(recieved.combatLoser/10 == playerNo){
                    unitSelected = 0;
                }
            }

            //Om det var ok att flytta enhetan
            if(recieved.okToMove == 1){
                unitIsMoving = 1;
                unitInArray = findLocalUnitWithID(units, toSend.unitID, numberOfUnits);
                //Flytta enheten lite
                moveLocalUnit(units[unitInArray], toSend.direction);
                printf("Unit moved to X: %d Y: %d\n", units[unitInArray]->x ,units[unitInArray]->y);
                //Detta uttryck kommer att bli sant d� enheten st�r mitt i en ruta (g�tt 64 pixlar)
                if(units[unitInArray]->x % 64 == 0 && units[unitInArray]->y % 64 == 0){
                    unitIsMoving = 0;
                    recieved.okToMove = 0;
                }
            }

            //Det blir n��sta spelares tur om alla enheter har g�tt klart
            if(recieved.nextPlayerTurn == 1){
                recieved.nextPlayerTurn = 0;
                playerTurn++;
                putMsg(chatsock,"/NEXT");
                if(playerTurn > playersInGame)
                    playerTurn = 1;
            }

            //visa interface
            apply_surface(0,640,GUIsheet,screen,NULL);

            //visa text
            apply_surface(100,150,message,screen,NULL);
            render_text(3); //current_menu=3 vid ingame

            //Om spelet �r slut
            if(recieved.gameOver == 1){
                SDLNet_TCP_Recv(sd, &recHSList, sizeof(recHSList));

                load_hiscores(recHSList);

                apply_surface(0,0,hiscore,screen,NULL);

                for (i=0; i<10; i++){
                    message = TTF_RenderText_Solid(lobby_font,hiscore_txt[i].str,textColorWhite);
                    apply_surface(hiscore_txt[i].x_pos,hiscore_txt[i].y_pos,message,screen,NULL);
                    message = TTF_RenderText_Solid(lobby_font,hiscore_num[i].str,textColorWhite);
                    apply_surface(hiscore_num[i].x_pos,hiscore_num[i].y_pos,message,screen,NULL);
                }
            }

            //Render current screen contents
            if(SDL_Flip(screen) == -1) {
                return 1;
            }

            SDL_Delay(50);
        }

        //PASSIVT L�GE
        while (quit==0 && playerNo != playerTurn) {

            startTicks = SDL_GetTicks();

            //Ta in tangentbordstryckningar
            input = handleKeyInput(input_mode2);

            if (input==14 && input_mode2==0) {
                input_mode2 = 1; //Textinput-l�get aktiverat
                SDL_EnableUNICODE(SDL_ENABLE);
                strcpy(gamechattext[4].str,"");
                gamechattext[4].index = 0; //Nollst�ll str�ngens l�ngd
                current_form = 6;
                fprintf(stderr,"Chat-mode aktiverat! (menu=%d) \n",current_menu); //debug
            }
            if (input==16) {
                putMsg(chatsock,gamechattext[4].str);
                SDL_EnableUNICODE(SDL_DISABLE);
                input = -1;
                input_mode2 = 0;
                gamechattext[4].index = 0;
                strcpy(gamechattext[4].str,"");
                fprintf(stderr,"Input klar \n"); //debug
            }

            if(input >= UP && input <= LEFT){
                moveCam(camera, input);
            }

            if(input == QUIT){
                quit = 1;
            }
            //S�tt input till -1 s� att knapptryckningarna inte "ligger kvar"
            if(input != 14)
                input = -1;

            //Kontrollera socketset efter data
            SDLNet_CheckSockets(set, 0);

            //Om det finns data i socketsetet och ingen enhet r�r sig
            if(SDLNet_SocketReady(sd) &&!unitIsMoving){
                printf("Something on socket\n");
                SDLNet_TCP_Recv(sd, &recieved, sizeof(recieved));

                printf("Mottagen struct: %d %d %d %d %d %d %d\n",
                recieved.okToMove, recieved.combat, recieved.combatLoser, recieved.unitID, recieved.moveDirection,
                recieved.nextPlayerTurn, recieved.disconnectedPlayer);
            }
            if(recieved.okToMove == 1){
                unitIsMoving = 1;
                unitInArray = findLocalUnitWithID(units, recieved.unitID,numberOfUnits);
                printf("unitInArray: %d\n", unitInArray);
                moveLocalUnit(units[unitInArray], recieved.moveDirection);
                printf("Unit moved to X: %d Y: %d\n", units[unitInArray]->x ,units[unitInArray]->y);
                if(units[unitInArray]->x % 64 == 0 && units[unitInArray]->y % 64 == 0){
                    unitIsMoving = 0;
                    recieved.okToMove = 0;
                }
            }

             if(recieved.combat != 1 && recieved.combat != 0){
                printf("Combat: %d \ncombatLoser: %d \n", recieved.combat, recieved.combatLoser);
                deadUnit = findLocalUnitWithID(units, recieved.combatLoser, numberOfUnits);
                units[deadUnit]->dead = 1;
                printf("Combat occured, unit %d\n", deadUnit);
                if(recieved.combatLoser/10 == playerNo){
                    unitSelected = 0;
                }
            }

            if(recieved.nextPlayerTurn == 1 && !unitIsMoving){
                recieved.nextPlayerTurn = 0;
                    playerTurn++;
                    if(playerTurn > playersInGame){
                        playerTurn = 1;
                    }
            }

             SDL_FillRect(screen,&screen->clip_rect,SDL_MapRGB(screen->format,0xFF,0xFF,0xFF));
            //Add clips to the screen (background)
            render_screen(camera,gamearea);

            //Rita ut enheter
            for(i = 0; i < numberOfUnits; i++){
                if(units[i]->dead == 0){
                    clipNo = get_clip(units[i]);
                    apply_surface(units[i]->x-camera->x, units[i]->y-camera->y, unitmap, screen,&unit_clip[clipNo]);
                }
            }


            //visa interface
            apply_surface(0,640,GUIsheet,screen,NULL);

            //visa text
            apply_surface(100,150,message,screen,NULL);
            render_text(3); //current_menu=3 vid ingame

            //Ta emot highscore om spelet �r �ver
            if(recieved.gameOver == 1){
                SDLNet_TCP_Recv(sd, &recHSList, sizeof(recHSList));

                load_hiscores(recHSList);

                apply_surface(0,0,hiscore,screen,NULL);

                for (i=0; i<10; i++){
                    message = TTF_RenderText_Solid(lobby_font,hiscore_txt[i].str,textColorWhite);
                    apply_surface(hiscore_txt[i].x_pos,hiscore_txt[i].y_pos, message,screen,NULL);
                    message = TTF_RenderText_Solid(lobby_font,hiscore_num[i].str,textColorWhite);
                    apply_surface(hiscore_num[i].x_pos,hiscore_num[i].y_pos,message,screen,NULL);
                }
            }

            //Render current screen contents
            if(SDL_Flip(screen) == -1) {
                return 1;
            }

            SDL_Delay(50);
            fflush(stdout);
        }
    }

    //Meddela chatservern att en spelare har disconnectat
    putMsg(chatsock,"/QUIT");

    //St�da upp
    for(i = 0; i < numberOfUnits; i++){
        free(units[i]);
    }

    free(camera);

    free(mouseC);

    clean_up();

    SDL_FreeSurface(background);

    //Free the surfaces
    SDL_FreeSurface(GUIsheet);
    SDL_FreeSurface(screen);
    SDL_FreeSurface(message);
    SDL_FreeSurface(waiting);

    //Close the font
    TTF_CloseFont( lobby_font );

    TTF_CloseFont(ingame_font);

    //Quit SDL_ttf
    TTF_Quit();

    SDL_Quit();
    exit(0);
}

int initgfx(){
    if(SDL_Init(SDL_INIT_EVERYTHING) == -1){
        return 0;
    }

    //Init SDL_ttf
    if (TTF_Init() == -1) {
        return 0;
    }
    SDL_WM_SetIcon(SDL_LoadBMP("icon.bmp"), NULL);
    screen = SDL_SetVideoMode(SCREEN_WIDTH,SCREEN_HEIGHT,SCREEN_BPP,SDL_HWSURFACE);
    if(screen==NULL){
        return 0;
    }

    //Initiera det som beh�vs f�r att spela upp ljud (SDL_mixer)
    if(Mix_OpenAudio( 22050, MIX_DEFAULT_FORMAT, 2, 4096 ) == -1 ){
        return 0;
    }

    SDL_WM_SetCaption("Vansquisher",NULL);
    text = NULL; //Initialize the surface

    return 1;
}

void clean_up() {
    SDL_FreeSurface(text);
    SDL_FreeSurface(mainmenu);
    SDL_FreeSurface(lobby);
    SDL_FreeSurface(message);
    TTF_CloseFont(lobby_font);
    TTF_CloseFont(ingame_font);
    SDL_FreeSurface( bsod_background );
    Mix_FreeChunk( roll_fast );
    Mix_FreeChunk( roll_slow );
    Mix_FreeMusic( music );
    TTF_CloseFont( font );
    Mix_CloseAudio();
    TTF_Quit();
    SDL_Quit();
    exit(0);
}
int load_gui_files(int opts)
{

    if(opts==1){
        lobby = load_image("gfx/lobbypink.png");
        mainmenu = load_image("gfx/mainmenu_pink.png");
        hiscore = load_image("gfx/Highscorespink.png");
        GUIsheet = load_image("gfx/guipink.png");

    }else{
        lobby = load_image("gfx/lobby.png");
        mainmenu = load_image("gfx/mainmenu.png");
        hiscore = load_image("gfx/Highscores.png");
        //Load the button sprite sheet
        GUIsheet = load_image("gfx/gui.png");
    }

    //Om det inte gick bra att ladda GUI-bilden
    if ( GUIsheet == NULL ) {
        return 0;
    }
    lobby_font = TTF_OpenFont("gfx/Impact.ttf",24);
    ingame_font = TTF_OpenFont("gfx/Impact.ttf",16);
    font = TTF_OpenFont( "gfx/lucon.ttf", 14 );
    bsod_background = load_image( "gfx/bluescreen_background.png" );

    //Om det inte gick att ladda in bakgrunden
    if( bsod_background == NULL ){
        return 0;
    }

    //Om det inte3 gick att ladda in fonten
    if( font == NULL ){
        return 0;
    }

    //Ladda bakgrundsmusiken
    music = Mix_LoadMUS( "sound/power-juice.wav" );
    if( music == NULL ){
        return 0;
    }

    //Ladda ljudeffekterna
    roll_fast = Mix_LoadWAV( "sound/fast.wav" );
    roll_slow = Mix_LoadWAV( "sound/slow.wav" );
    if( ( roll_fast == NULL ) || ( roll_slow == NULL ) ){
        return 0;
    }

    if (mainmenu==NULL) {
        return false;
    }
    if (lobby==NULL) {
        return false;
    }
    if (hiscore==NULL) {
        return false;
    }
    if (lobby_font==NULL) {
        return false;
    }
    if (ingame_font==NULL) {
        return false;
    }

    //If everything loaded fine
    return 1;
}

void fill_out_forms()
{
    //chatinput-rutan
    formtext[0].x_pos=box[0].x; //64
    formtext[0].y_pos=box[0].y; //656
    //ip-rutan
    formtext[1].x_pos=box[1].x; //640
    formtext[1].y_pos=box[1].y; //96
    //port-rutan
    formtext[2].x_pos=box[2].x; //640
    formtext[2].y_pos=box[2].y; //160
    //namn-rutan
    formtext[3].x_pos=box[3].x; //640
    formtext[3].y_pos=box[3].y; //224
    //varningstext
    formtext[4].x_pos=64;
    formtext[4].y_pos=716;
    //map-rutan
    formtext[5].x_pos=640;
    formtext[5].y_pos=432;

    //chat-rutan ingame
    gamechattext[0].x_pos=12;   //rad 1
    gamechattext[0].y_pos=652;

    gamechattext[1].x_pos=12;   //rad 2
    gamechattext[1].y_pos=669;

    gamechattext[2].x_pos=12;   //rad 3
    gamechattext[2].y_pos=686;

    gamechattext[3].x_pos=12;   //rad 4
    gamechattext[3].y_pos=703;

    gamechattext[4].x_pos=12;   //input-f�ltet
    gamechattext[4].y_pos=743;

    //chatoutput-rutan
    int i,x=64,y=64;
    for (i=17; i>=0; i--) {
    	chattext[i].x_pos=x;
    	chattext[i].y_pos=y;
    	y+=32;
    }
}

void render_text(int menu)
{
    //g� igenom alla textstr�ngar och rendera dom som inte �r tomma.
    int index;
    if (menu==2) {
        //g� igenom all text i textf�lten, rendera dem som inte �r tomma.
        for (index=0; index<6; index++) {
            if (strlen(formtext[index].str)>0) {
                SDL_FreeSurface(text);
                text = TTF_RenderText_Solid(lobby_font,formtext[index].str,textColorBlack);
                apply_surface(formtext[index].x_pos,formtext[index].y_pos,text,screen,NULL);
            }
        }

        //g� igenom all text i lobby-chatrutan, rendera dem som inte �r tomma.
        for (index=0; index<18; index++) {
            if (strlen(chattext[index].str)>0) {
                SDL_FreeSurface(text);
                text = TTF_RenderText_Solid(lobby_font,chattext[index].str,textColorBlack);
                apply_surface(chattext[index].x_pos,chattext[index].y_pos,text,screen,NULL);
            }
        }
    }
    if (menu==3) {
        //g� igenom all text i ingame-chatrutan, rendera dem som inte �r tomma.
        for (index=0; index<5; index++) {
            if (strlen(gamechattext[index].str)>0) {
                SDL_FreeSurface(text);
                text = TTF_RenderText_Solid(ingame_font,gamechattext[index].str,textColorBlack);
                apply_surface(gamechattext[index].x_pos,gamechattext[index].y_pos,text,screen,NULL);
            }
        }
    }
}

// Hanterar mustryckningar i lobbyn.
// Parametern menu visar vilken meny anv�ndaren befinner sig i.
// Boxnumrering: 0=chat, 1=ip, 2=port, 3=namn, 4=join, 5=select, 6=start, 7=change
void handle_events(int menu, int *current_map, int *input_mode, int *name_OK, int *joinGame, int *startGame) {
    int x=0,y=0; //The mouse offsets

    if (menu==2) {
        if (event.type==SDL_MOUSEBUTTONDOWN) { //Om en musknapp har tryckts ner
            if (event.button.button==SDL_BUTTON_LEFT) { //Om det var v�nster musknapp
                //H�mta musens offsets
                x = event.button.x;
                y = event.button.y;

                //Om musen  var �ver en av knapparna
                if ((x>box[0].x) && (x<box[0].x+box[0].w) && (y>box[0].y) && (y<box[0].y+box[0].h)) {
                    if (*input_mode==false) {
                        if (*name_OK==true) {
                            *input_mode = true;
                            current_form = 0;
                            SDL_EnableUNICODE(SDL_ENABLE);
                        }
                    }
                } else if ((x>box[1].x) && (x<box[1].x+box[1].w) && (y>box[1].y) && (y<box[1].y+box[1].h)) {
                    if (*input_mode==false) {
                        *input_mode = true;
                        current_form = 1;
                        SDL_EnableUNICODE(SDL_ENABLE);
                    }
                } else if ((x>box[2].x) && (x<box[2].x+box[2].w) && (y>box[2].y) && (y<box[2].y+box[2].h)) {
                    if (*input_mode==false) {
                        *input_mode = true;
                        current_form = 2;
                        SDL_EnableUNICODE(SDL_ENABLE);
                    }
                } else if ((x>box[3].x) && (x<box[3].x+box[3].w) && (y>box[3].y) && (y<box[3].y+box[3].h)) {
                    if (*input_mode==false) {
                        *input_mode = true;
                        current_form = 3;
                        SDL_EnableUNICODE(SDL_ENABLE);
                    }
                } else if ((x>box[4].x) && (x<box[4].x+box[4].w) && (y>box[4].y) && (y<box[4].y+box[4].h)) {
                    if (ip_ok==1 && port_ok==1) {
                        //anslut till server
                        *joinGame = 1;
                        //fprintf(stderr,"Ansluter till server \n");
                    }
                } else if ((x>box[5].x) && (x<box[5].x+box[5].w) && (y>box[5].y) && (y<box[5].y+box[5].h)) {
                    //byt karta
                    char temp[20];
                    strcpy(temp,"Map selected by host: ");
                    strcat(temp,bg_maps[*current_map]);
                    add_chat_row(temp);
                } else if ((x>box[6].x) && (x<box[6].x+box[6].w) && (y>box[6].y) && (y<box[6].y+box[6].h)) {
                    //starta spelet
                    *startGame = 1;
                    current_menu = 3;
                    Mix_PlayMusic(music,-1); //starta musiken
                    //fprintf(stderr,"Startar spelet \n");
                } else if ((x>box[7].x) && (x<box[7].x+box[7].w) && (y>box[7].y) && (y<box[7].y+box[7].h)) {
                    //byta karta
                    (*current_map)++;
                    if (*current_map>2) {
                        *current_map = 0;
                    }
                    strcpy(formtext[5].str,bg_maps[*current_map]);
                }
            }
        }
    }
}

// Hanterar mustryckningar f�r ingame GUI'n.
// Parametern �r  en strukt d�r koordinater f�r musklick finns
int handle_events_gui(mouseClick *mouseC) {
    //musens offsets
    int x=0,y=0;

    //Ta emot musens koordinater
    x = mouseC->xPos;
    y = mouseC->yPos;

    //Vilken ruta pekade musen p�?
    if ((x>box[0].x) && (x<box[0].x+box[0].w) && (y>box[0].y) && (y<box[0].y+box[0].h)) {
        //chatrutan
        return 1;
    } else if ((x>box[1].x) && (x<box[1].x+box[1].w) && (y>box[1].y) && (y<box[1].y+box[1].h)) {
        //no music
        return 2;
    } else if ((x>box[2].x) && (x<box[2].x+box[2].w) && (y>box[2].y) && (y<box[2].y+box[2].h)) {
        //no sound
        return 3;
    } else if ((x>box[3].x) && (x<box[3].x+box[3].w) && (y>box[3].y) && (y<box[3].y+box[3].h)) {
        //skip unit
        return 4;
    } else if ((x>box[4].x) && (x<box[4].x+box[4].w) && (y>box[4].y) && (y<box[4].y+box[4].h)) {
        //surrender
        return 5;
    } else if ((x>box[5].x) && (x<box[5].x+box[5].w) && (y>box[5].y) && (y<box[5].y+box[5].h)) {
        //end turn
        return 6;
    }
    return 0;
}
